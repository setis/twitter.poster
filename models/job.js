/**
 * Created by alex on 10.04.16.
 */
module.exports = function (mongoose, connection, models, plugins) {
    var Schema = mongoose.Schema;

    var post_many = new Schema({
        source: {
            id: {type: Schema.Types.ObjectId, index: true},
            model: {type: String, index: true}
        },
        accounts: [
            {type: Schema.Types.ObjectId}
        ],
        filter: {
            country: {type: String},
            lang: {type: String},
            count: {type: Number, default: 0}
        },
        data: {
            msg: {type: String, required: true},
            img: []
        }
    }, {_id: false});
    var post_once = new Schema({
        source: {
            id: {type: Schema.Types.ObjectId, index: true},
            model: {type: String, index: true}
        },
        data: {
            msg: {type: String, required: true},
            img: [
                {type: String}
            ],
            account: {type: Schema.Types.ObjectId}
        }
    }, {_id: false});
    var job = new Schema({
        task: {type: String, index: true},
        subtask: [
            {type: Schema.Types.ObjectId, index: true}
        ],
        source: {
            id: {type: Schema.Types.ObjectId, index: true},
            model: {type: String, index: true}
        },
        status: {type: Boolean, default: false, index: true},
        try: {type: Number, default: 0, index: true},
        time: {type: Date},
        date: {type: Date, default: Date.now, index: true},
        data: {type: Schema.Types.Mixed},
        result:{type: Schema.Types.Mixed}
    });
    // schema.pre('init', function (next, data) {
    //     switch (data.task) {
    //         case 'post.many':
    //             data.data = post_many(data.data);
    //             break;
    //         case 'post.once':
    //             data.data = post_once(data.data);
    //             break;
    //     }
    //     next();
    // });
    models['job'] = connection('static').model('job', job);
};