/**
 * Created by alex on 10.04.16.
 */
module.exports = function (mongoose, connection, models, plugins) {
    var Schema = mongoose.Schema,
        model = new Schema({
            login: {type: String, required: true, unique: true, index: true},
            password: {type: String, required: true},
            access_token_key: {type: String},
            access_token_secret: {type: String},
            bitly: {type: String},
            country: {type: String, index: true},
            lang: {type: String, index: true},
            ip: {type: String},
            ua: {type: String},
            time: {type: Date, default: Date.now},
            error: {
                msg: {type: String},
                time: {type: Date}
            },
            info: {type: Schema.Types.Mixed}
        });
    models['account'] = connection('static').model('account', model);
};