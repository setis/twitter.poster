/**
 * Created by alex on 10.04.16.
 */
module.exports = function (mongoose, connection, models, plugins) {
    var Schema = mongoose.Schema,
        model = new Schema({
            name: {type: String},
            cron: [
                {type: String}
            ],
            limit: {
                begin: {type: Date, default: Date.now},
                end: {type: Date}
            },
            accounts: [
                {type: Schema.Types.ObjectId}
            ],
            filter: {
                country: {type: String},
                lang: {type: String},
                count: {type: Number}
            },
            data: {
                msg: {type: String, required: true},
                img: [

                    {type: String}
                ]
            }


        });
    models['cron'] = connection('static').model('cron', model);
};