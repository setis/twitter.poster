/**
 * Created by alex on 19.04.16.
 */
var casper = require("casper").create({
    viewportSize: {
        width: 1024,
        height: 768
    },
    timeout: 60e3,
    // logLevel: "debug",
    verbose: false
});
var
    login = "is.set@mail.ru",
    password = "z234339013";
var data = {};


casper.on('page.initialized', function (page) {
    page.evaluate(function () {
        (function () {
            var create = document.createElement;
            document.createElement = function (tag) {
                var elem = create.call(document, tag);
                if (tag === "video") {
                    elem.canPlayType = function () {
                        return "probably";
                    };
                }
                return elem;
            };
        })();
    });
});
casper
    .on('http.status.501', function (resource) {
        this.echo(JSON.stringify({status: false, msg: 'http.status.501'}));
        this.exit();
    })
    .on('page.error', function (msg) {
        this.echo(JSON.stringify({status: false, msg: msg}));
        this.exit();
    })
    // .on('resource.requested', function (resource) {
    //     console.log(resource.url);
    // })
    .on('error', function (msg) {
        if (msg === 'CasperError: Errors encountered while filling form: no field matching names selector "session[username_or_email]" in form; no field matching names selector "session[password]" in form') {
            this.echo(JSON.stringify({status: false, msg: 'not form loaded'}));
        } else {
            this.echo(JSON.stringify({status: false, msg: msg}));
        }
        this.capture("logs/screen-" + login + ".png");
        var fs = require('fs');
        fs.write('logs/bug-' + login + '.html', this.getPageContent());
        this.exit();
    });
casper.start();
casper

    .open('https://twitter.com/login')

    .wait(1e3, function () {
        this.evaluate(function (login, password) {
            var form = document.querySelector('form[action="https://twitter.com/sessions"]');
            form.querySelector("input[name='session[username_or_email]']").value = login;
            form.querySelector("input[name='session[password]']").value = password;
            form.querySelector('input[type="submit"]').click();
        }, login, password);
    })
    .wait(5e3)
    .wait(750, function () {
        var re = /\/login\/error\?username_or_email/gmi;
        if (re.test(this.getCurrentUrl())) {
            this.echo(JSON.stringify({error: 'login and pasword'}));
            this.exit();
        }
        var re2 = /\/account\/login_challenge/gmi
        if (re2.test(this.getCurrentUrl())) {
            this.echo(JSON.stringify({error: 'confirmation code'}));
            this.exit();
        }

    })
    .wait(3e3, function () {
        this.evaluate(function (msg) {
            document.getElementById('global-new-tweet-button').click();
            document.querySelector('#tweet-box-global').innerHTML = `<div>${ msg }</div>`;
        }, 'asdkkjsd' + (new Date()).toString());

    })
    .then(function () {
        this.page.uploadFile('#global-tweet-dialog-dialog > div.modal-content > div.modal-tweet-form-container > form > div.TweetBoxToolbar > div.TweetBoxExtras.tweet-box-extras > span.TweetBoxExtras-item.TweetBox-mediaPicker > div > div > label > input[type="file"]', '/home/alex/Изображения/VirtualBox_winXp(32)_28_01_2016_22_32_41.png');
    })
    .wait(10e3, function () {
        this.evaluate(function () {
            document.querySelector('#global-tweet-dialog-dialog > div.modal-content > div.modal-tweet-form-container > form > div.TweetBoxToolbar > div.TweetBoxToolbar-tweetButton.tweet-button > button').click();
            // document.querySelector('input[type="file"]').click();
        });
    })
    .wait(3e3, function () {
        var msg = this.evaluate(function () {
            return $('#message-drawer > div > div > span').text();
        }).trim();
        data.msg = msg;
        switch (msg) {
            case "Ваш твит опубликован!":
                data.status = true;
                break;
            default:
                data.status = false;
                break;
        }
    })
    .run(function () {
        this.capture("logs/screen-" + login + "-finish.png");
        var fs = require('fs');
        fs.write('logs/bug-' + login + '-finish.html', this.getPageContent());
        this.echo(JSON.stringify(data));
        this.exit();
    });
