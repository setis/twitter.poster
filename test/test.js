/**
 * Created by alex on 09.07.16.
 */
var page = require('webpage').create({
    viewportSize: {
        width: 1024,
        height: 768
    },
    timeout: 60e3,
    logLevel: "debug",
    verbose: true
});
page.settings.userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36';
page.settings.javascriptEnabled = true;
page.settings.loadImages = false; //Script is much faster with this field set to false
// phantom.cookiesEnabled = true;
// phantom.javascriptEnabled = true;
var steps = [];
var testindex = 0;
var loadInProgress = false;
var auth = 'https://mobile.twitter.com/session/new', post = 'https://mobile.twitter.com/compose/tweet', login = "is.set@mail.ru", password = "z234339013";
page.onConsoleMessage = function (msg) {
    //    phantom.outputEncoding = "utf-8";
    console.log(msg);
};
steps = [
    //Step 1 - Open Twitter login page
    function () {
        console.log('Step 1 - Open Twitter Login page');
        page.open('https://twitter.com/login', function (status) {
        });
    },
    function () {
        page.onInitialized = function () {
            page.evaluate(function () {
                var create = document.createElement;
                document.createElement = function (tag) {
                    var elem = create.call(document, tag);
                    if (tag === "video") {
                        elem.canPlayType = function () { return "probably"; };
                    }
                    return elem;
                };
            });
        };
    },
    //Step 2 - Populate and submit the login form
    function () {
        console.log('Step 2 - Populate and submit the login form');
        page.evaluate(function (login, password) {
            var form = document.querySelector('form[action="https://twitter.com/sessions"]');
            form.querySelector("input[name='session[username_or_email]']").value = login;
            form.querySelector("input[name='session[password]']").value = password;
            form.querySelector('input[type="submit"]').click();
        }, login, password);
    },
    function () {
        console.log("Step 3 ");
        page.evaluate(function () {
            document.getElementById('global-new-tweet-button').click();
        });
        setTimeout(function () {
            // page.uploadFile('.modal-tweet-form-container > form input[type="file"]', '/home/alex/Изображения/VirtualBox_winXp(32)_28_01_2016_22_32_41.png');
            page.evaluate(function (msg) {
                document.querySelector('#tweet-box-global').innerHTML = "<div>" + msg + "</div>";
                document.querySelector('.modal-tweet-form-container > form > div.TweetBoxToolbar > div.TweetBoxToolbar-tweetButton.tweet-button > button').click();
            }, 'sdfsdfsdf' + (new Date()).toString());
        }, 3e3);
    },
    function () {
        setTimeout(function () {
            page.render('test.png');
        }, 5e3);
    }
];
/**********END STEPS THAT FANTOM SHOULD DO***********************/
//Execute steps one by one
interval = setInterval(executeRequestsStepByStep, 150);
function executeRequestsStepByStep() {
    if (loadInProgress == false && typeof steps[testindex] == "function") {
        //console.log("step " + (testindex + 1));
        steps[testindex]();
        testindex++;
    }
    if (typeof steps[testindex] != "function") {
    }
}
/**
 * These listeners are very important in order to phantom work properly. Using these listeners, we control loadInProgress marker which controls, weather a page is fully loaded.
 * Without this, we will get content of the page, even a page is not fully loaded.
 */
page.onLoadStarted = function () {
    loadInProgress = true;
    console.log('Loading started');
};
page.onLoadFinished = function () {
    loadInProgress = false;
    console.log('Loading finished');
};
page.onConsoleMessage = function (msg) {
    console.log(msg);
};
