/**
 * Created by alex on 10.04.16.
 */

var logger = require('./logger'),
    loading = require('./loading'),
    cfg = require('../../config/cron.json'),
    async = require('async'),
    moment = require('moment'),
    geoip2 = require('node-geoip2'),
    random_useragent = require('random-useragent'),
    ObjectId = require('mongoose').Types.ObjectId,
    gearmanode = require('gearmanode'),
    clone = require('node-v8-clone').clone,
    unique = require('array-unique'),
    client = gearmanode.client(cfg.gearman),
    geoip;
gearmanode.Client.logger.transports.console.level = 'info';
if (cfg.geoip) {
    try {
        geoip2.init(cfg.geoip);
        geoip = geoip2.lookup;
        logger.info('geoip path:', cfg.geoip);
    } catch (e) {
        logger.error('geoip not load %s', cfg.geoip, e);
        geoip = geoip2.lookupSimple;
        logger.info('geoip net');
    }
}
setInterval(geoip2.cleanup, 1e3 * 60 * 15);
function rand(low, high) {
    return Math.floor(Math.random() * (high - low + 1) + low);
}
require('./loader')(function (core) {
    var account_country = new loading(function (callback) {
            core.models.odm.account.find(
                {country: {$exists: false}}
                , function (err, result) {
                    if (err) {
                        callback(err);
                        return;
                    }
                    if (result.length === 0) {
                        callback();
                        return;
                    }
                    var tasks = result.map(function (doc) {
                        return function (callback) {
                            geoip(doc.ip, function (err, result) {
                                if (err) {
                                    callback(err);
                                    return;
                                }
                                core.models.odm.account.update({_id: doc._id}, {country: result.country.iso_code}, callback);
                            });
                        };
                    });
                    async.parallel(tasks, callback);
                });
        },
        {
            timeout: {
                error: 30,
                not_found: 60
            },
            interval: 60,
            // interval: 1,
            msg: {
                error: "account.country time:%ss",
                success: "account.country time:%ss"
            }
        },
        'account.country');
    var auth = new loading(function (callback) {
            core.models.odm.account.find({
                    country: {$exists: true},
                    "error.msg": {
                        $nin: ['login and pasword', 'confirmation code', 'not form loaded']
                    },
                    $or: [
                        {
                            "error.msg": "not auth",
                            "error.time": {
                                $gte: new Date((new Date()).getTime() - 15 * 60 * 1e3),
                            }
                        },
                        {lang: {$exists: false}},
                        {access_token_key: {$exists: false}},
                        {access_token_secret: {$exists: false}},

                        // {bitly: {$exists: false}},
                    ]
                },
                function (err, result) {
                    if (err) {
                        callback(err);
                        return;
                    }
                    if (result.length === 0) {
                        logger.error('not found accounts');
                        callback();
                        return;
                    }
                    logger.info('account found %d', result.length);
                    async.eachLimit(
                        result,
                        100,
                        function (doc, callback) {
                            client.submitJob('twitter.auth',
                                JSON.stringify(doc),
                                {
                                    background: true,
                                    priority: 'NORMAL',
                                    unique: 'twitter.auth#' + doc._id.toString()
                                });
                            setTimeout(callback, 350);
                        },
                        callback);
                }
            )
            ;

        },
        {
            timeout: {
                error: 30,
                not_found: 60
            },
            interval: 30,
            // interval: 1,
            msg: {
                error: "twitter.auth time:%ss",
                success: "twitter.auth time:%ss"
            }
        },
        'twitter.auth');
    var fixua = new loading(function (callback) {
            var model = core.models.odm.account;
            model.find({
                    "error.msg": 'not form loaded',

                },
                function (err, result) {
                    if (err) {
                        callback(err);
                        return;
                    }
                    if (result.length === 0) {
                        logger.error('not found accounts');
                        callback();
                        return;
                    }
                    logger.info('account found %d', result.length);
                    async.each(
                        result,
                        function (doc, callback) {
                            model.update(
                                {_id: doc._id},
                                {
                                    $set: {
                                        ua: // random_useragent.getRandom()
                                            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.93 Safari/537.36'
                                    },
                                    $unset: {error: true}
                                }, callback);
                        },
                        callback);
                }
            );

        },
        {
            timeout: {
                error: 30,
                not_found: 60
            },
            interval: 30,
            // interval: 1,
            msg: {
                error: "twitter.fixua time:%ss",
                success: "twitter.fixua time:%ss"
            }
        },
        'twitter.auth');
    var post_many = new loading(function (callback) {
        async.waterfall([
            function (callback) {
                core.models.odm.job.find(
                    {
                        task: "post.many",
                        date: {
                            $gte: new Date((new Date()).getTime() - 6 * 60 * 60 * 1e3),
                            $lte: new Date()
                        }
                    },
                    function (err, result) {
                        if (err) {
                            callback(err);
                            return;
                        }
                        if (result.length === 0) {
                            logger.info('twitter.post.many not found');
                            callback(new Error('job.find not found', 0));
                            return;
                        }
                        callback(null, result);
                    });
            },
            function (jobs, callback) {
                async.eachLimit(
                    jobs,
                    100,
                    function (job, callback) {
                        if (job.data.filter === undefined) {
                            callback();
                            return;
                        }
                        async.parallel({
                                create: function (callback) {
                                    var q = core.models.odm.account
                                        .find({
                                            country: job.data.filter.country,
                                            lang: job.data.filter.lang,
                                            access_token_key: {$exists: true},
                                            access_token_secret: {$exists: true},
                                            _id: {$nin: job.data.accounts}
                                            // bitly: {$exists: true}

                                        })
                                        .select('country lang');
                                    if (job.data.filter.count !== 0) {
                                        if (job.data.filter.country - job.data.accounts.length < 1) {
                                            callback();
                                            return;
                                        }
                                        q.limit(job.data.filter.country - job.data.accounts.length);
                                    }
                                    q.exec(function (error, result) {
                                        if (error) {
                                            callback(error);
                                            return;
                                        }
                                        async.map(
                                            result,
                                            function (doc, callback) {
                                                callback(null, doc._id);
                                            },
                                            callback
                                        );
                                    });
                                },
                                check: function (callback) {
                                    core.models.odm.job
                                        .find({
                                                task: "post.once",
                                                source: {
                                                    model: "job",
                                                    id: job._id
                                                }
                                            },
                                            function (error, result) {
                                                if (error) {
                                                    callback(error);
                                                    return;
                                                }
                                                async.waterfall([
                                                    function (callback) {
                                                        var dupl = {};
                                                        async.each(
                                                            result,
                                                            function (doc, callback) {
                                                                if (dupl[doc.data.account] === undefined) {
                                                                    dupl[doc.data.account] = [];
                                                                }
                                                                dupl[doc.data.account].push(doc._id);
                                                                callback();
                                                            },
                                                            function (err) {
                                                                callback(err, dupl);
                                                            }
                                                        );
                                                    },
                                                    function (arr, callback) {
                                                        if (Object.keys(arr).length === 0) {
                                                            callback(null, null);
                                                            return;
                                                        }
                                                        var duplication = [];
                                                        async.forEachOf(
                                                            arr,
                                                            function (jobs, account, callback) {
                                                                if (jobs.length > 1) {
                                                                    duplication.push.apply(duplication, jobs.slice(1));
                                                                }
                                                                callback();
                                                            },
                                                            function (err) {
                                                                if (err) {
                                                                    throw err;
                                                                }
                                                                if (duplication.length === 0) {
                                                                    callback(null, null);
                                                                    return;
                                                                }
                                                                core.models.odm.job.remove({
                                                                        _id: {$in: duplication},
                                                                        task: "post.once",
                                                                        source: {
                                                                            model: "job",
                                                                            id: job._id
                                                                        }
                                                                    },
                                                                    function (err) {
                                                                        if (err) {
                                                                            throw err;
                                                                        }
                                                                        callback(null, unique(Object.keys(arr)));
                                                                    }
                                                                );
                                                            }
                                                        );

                                                    }
                                                ], callback);

                                            });
                                }
                            },
                            function (err, results) {
                                if (err) {
                                    callback(err);
                                    return;
                                }
                                console.log(results);
                                if (results.create === null || results.create.length === 0) {
                                    callback();
                                    return;
                                }
                                var create = (results.check === null || results.check.length === 0) ? results.create : results.create.filter(function (v) {
                                    return (results.check.indexOf(v) === -1);
                                });
                                var data = {
                                    task: "post.once",
                                    data: clone(job.data.data, false),
                                    status: false,
                                    try: 0,
                                    source: {
                                        model: "job",
                                        id: job.id
                                    }
                                };
                                async.each(
                                    create,
                                    function (account, callback) {
                                        var data2 = clone(data, false)
                                        data2.data.account = account;
                                        core.models.odm.job.create(data,
                                            function (err) {
                                                if (err) {
                                                    console.log(err);
                                                }
                                                callback();
                                            }
                                        );
                                    },
                                    callback
                                );


                            });

                    },
                    callback);

            }
        ], function (error) {
            if (error) {
                if (error.message === 'job.find not found') {
                    logger.error(error.message);
                    callback();
                    return;
                }
                callback(error);
                return;
            }
            callback();
        });
    }, {
        timeout: {
            error: 30,
            not_found: 60
        },
        interval: 60,
        // interval: 1,
        msg: {
            error: "twitter.post.many time:%ss",
            success: "twitter.post.many time:%ss"
        }
    }, 'twitter.post.many');
    var post_once = new loading(function (callback) {
        core.models.odm.job.find(
            {
                "task": 'post.once',
                "status": false,
                $or: [
                    {"try": {$lte: 10000}},
                    {"try": {$exists: false}}
                ]
            },
            function (error, result) {
                if (error) {
                    logger.error(new Error(error.message, error.code));
                    callback(error);
                    return;
                }
                if (result.length === 0) {
                    logger.info('twitter.post not found');
                    callback();
                    return;
                }
                // var list = ['country', 'access_token_key', 'access_token_secret', 'bitly'].join(' ');
                var list = ['country', 'access_token_key', 'access_token_secret', 'login', 'password'].join(' ');
                async.each(
                    result,
                    function (doc, callback) {
                        var job = doc._doc;
                        if (job.data.account === undefined) {
                            callback();
                            return;
                        }
                        core.models.odm.account.findOne(
                            {
                                _id: job.data.account
                            })
                        .select(list)
                            .exec(function (error, result) {
                                if (error) {
                                    logger.error(new Error(error.message, error.code));
                                    callback(error);
                                    return;
                                }
                                if (result === null) {
                                    logger.error('twitter.post not found account ', job.data);
                                    callback();
                                    return;
                                }
                                var data3 = {
                                    account: result._doc,
                                    job: job._id,
                                    msg: job.data.msg,
                                    img: job.data.img
                                };
                                client.submitJob(
                                    'twitter.post',
                                    JSON.stringify(data3),
                                    {
                                        background: true,
                                        priority: 'LOW',
                                        unique: job._id.toString()
                                    });
                                callback();
                            });
                    },
                    function (error) {
                        if (error) {
                            logger.error('twitter.post', new Error(error.message, error.code));
                            callback(error);
                            return;
                        }
                        callback();
                    });

            });
    }, {
        timeout: {
            error: 30,
            not_found: 60
        },
        interval: 60,
        msg: {
            error: "twitter.post.once time:%ss",
            success: "twitter.post.once time:%ss"
        }
    }, 'twitter.post.one');
});
