'use strict';
var fs = require('fs'),
    path = require('path'),
    logger = require('./logger'),
    mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment'),
    cfg = require('../../config/admin.json'),
    loader = require('../admin/kernel/loader'), models = {
        odm: {},
        orm: {}
    },
    connections = {
        mongoose: {},
        orm: {}
    },
    clients = {};
var eventCount = 1;
var count = Object.keys(cfg.mongoose).length;
var dirOdm = path.normalize(path.join(__dirname, '/../../models'));
for (var name in cfg.mongoose) {
    var load = function () {
        loader.odm(mongoose, connections.mongoose, dirOdm, models.odm, {autoIncrement: autoIncrement}, {
            error: function (err, path) {
                logger.info('odm path: %s %s', path, err);
            },
            success: function (path) {
                logger.info('odm load %s', path);
            },
            done: function () {
                eventCount--;
            }
        });
    };
    (function (cfg, name) {
        var connection = mongoose.createConnection(cfg.connect, cfg.options);
        connections.mongoose[name] = connection;
        connection.once('open', function (connection) {
            logger.info('mongoose connect: ' + name);
            count--;
            if (!count) {
                load();
            }
        });
        connection.on('error', function (err) {
            logger.error('not connect name: %s mongoose error: %s', name, err);
        });
        autoIncrement.initialize(connection);
    })(cfg.mongoose[name], name);

}
module.exports = function (cb) {
    var id = setInterval(function () {
        if (eventCount) {
            return;
        }
        clearInterval(id);
        cb({models: models, connections: connections, clients: clients, logger: logger});
    }, 50);
    if (!eventCount) {
        cb({models: models, connections: connections, clients: clients, logger: logger});
    }
};
