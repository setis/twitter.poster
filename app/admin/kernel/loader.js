_reconnect = 3000;
function mysql(cfg, db, reconnect) {
    var mysql = require('mysql');
    if (reconnect === undefined) {
        reconnect = _reconnect;
    }
    function connect() {
        db = mysql.createPool({
            connectionLimit: cfg.connectionLimit,
            host: cfg.host,
            user: cfg.user,
            password: cfg.password,
            database: cfg.database,
            port: cfg.port,
            debug: cfg.debug
        });
        db.connect(function (err) {
            if (err) {
                console.error(err);
                if (reconnect) {
                    setTimeout(connect, reconnect);
                }
            }
        });
    }
    db.on('error', function (err) {
        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
            connect();
            return;
        }
        console.error('not connect mysql error: ', err.code);
    });
    db.on('connection', function (connection) {
//  connection.query('SET SESSION auto_increment_increment=1')
        console.info('mysql connect');
    });
    connect();
}
function odm(mongoose, connections, dir, models, plugins, events) {
    var fs = require('fs'),
            path = require('path'),
            EventEmitter = require('events').EventEmitter,
            event = new EventEmitter;
//    var mongoose = require('mongoose');
    event.setMaxListeners(0);
    if (event !== undefined) {
        for (var name in events) {
            event.on(name, events[name]);
        }
    }
    function connection(name) {
        if (connections[name] === undefined) {
            throw new Error('not odm connection: ' + name);
        }
        return connections[name];
    }
    fs.readdir(dir, function (err, result) {
        if (err) {
            event.emit('error', err, dir);
            return false;
        }
        var i, link, f;
        for (i in result) {
            link = path.join(dir, result[i]);
            f = false;
            try {
                require(link)(mongoose, connection, models, plugins);
            } catch (e) {
                event.emit('error', e, link);
                f = true;
            }
            if (!f) {
                event.emit('success', link);
            }
        }
        event.emit('done');
    });
}
function odmSync(mongoose, connections, dir, models, plugins, events) {
    var fs = require('fs'),
            path = require('path'),
            EventEmitter = require('events').EventEmitter,
            event = new EventEmitter;
    event.setMaxListeners(0);
    if (event !== undefined) {
        for (var name in events) {
            event.on(name, events[name]);
        }
    }
    function connection(name) {
        if (connections[name] === undefined) {
            throw new Error('not odm connection: ' + name);
        }
        return connections[name];
    }
    try {
        var result = fs.readdir(dir);
    } catch (e) {
        if (e) {
            event.emit('error', e, dir);
            return false;
        }
    }
    var i, link, f;
    for (i in result) {
        link = path.join(dir, result[i]);
        f = false;
        try {
            require(link)(mongoose, connection, models, plugins);
        } catch (e) {
            event.emit('error', e, link);
            f = true;
        }
        if (!f) {
            event.emit('success', link);
        }
    }
    event.emit('done');

}
function orm(connection, dir, models, events) {
    var fs = require('fs'),
            path = require('path'),
            EventEmitter = require('events').EventEmitter,
            event = new EventEmitter;
    if (event !== undefined) {
        for (var name in events) {
            event.on(name, events[name]);
        }
    }
    fs.readdir(dir, function (err, result) {
        if (err) {
            event.emit('error', err, dir);
            return false;
        }
        var i, link, f;
        for (i in result) {
            link = path.join(dir, result[i]);
            f = false;
            try {
                require(link)(connection, models);
            } catch (e) {
                event.emit('error', e, link);
                f = true;
            }
            if (!f) {
                event.emit('success', link);
            }
        }
        event.emit('done');
    });
}
function ormSync(connection, dir, models, events) {
    var fs = require('fs'),
            path = require('path'),
            EventEmitter = require('events').EventEmitter,
            event = new EventEmitter;
    if (event !== undefined) {
        for (var name in events) {
            event.on(name, events[name]);
        }
    }
    try {
        var result = fs.readdir(dir);
    } catch (e) {
        if (e) {
            event.emit('error', e, dir);
            return false;
        }
    }
    var i, link, f;
    for (i in result) {
        link = path.join(dir, result[i]);
        f = false;
        try {
            require(link)(connection, models);
        } catch (e) {
            event.emit('error', e, link);
            f = true;
        }
        if (!f) {
            event.emit('success', link);
        }
    }
    event.emit('done');
}
function mongodb(cfg, db) {
    var MongoClient = require('mongodb').MongoClient;
    MongoClient.connect(cfg, function (err, connect) {
        if (err) {
            console.error('not connect mongodb error:', err.code);
        }
        db = connect;
        console.log("mongodb connect");
    });

}
function gearman(cfg, level) {
    var gearmanode = require('gearmanode');
    if (level === undefined) {
        level = 'info';
    }
    gearmanode.Client.logger.transports.console.level = level;
    return gearmanode.client(cfg);
}

function event() {
    var zmq = require('zmq'), cluster = require('cluster');
    function sub(cfg, cb) {
        var socket = zmq.socket('sub');
        socket.identity = (cluster.isMaster) ? 'm#' + cfg.id : 'w#' + cfg.id + '-'.cluster.worker.id;
        socket.connect(cfg.connect);
        socket.subscribe(cfg.subscribe);
        socket.on('message', function (data) {
            handler(socket, data, cb);
        });
        return socket;
    }
    function pub(cfg) {
        var socket = zmq.socket('pub');
        socket.identity = (cluster.isWorker) ? 'w#' + cfg.id + '-' + cluster.worker.id : 'w#' + cfg.id;
        socket.connect(cfg.connect);
        return socket;
    }
    function handler(socket, data, cb) {
        var arr = data.toString().split(' ');
        arr[1] = JSON.parse(arr[1]);
        cb(arr[0], arr[1], socket);
    }
    function send(socket, subscribe, data) {
        socket.send(subscribe + ' ' + JSON.stringify(data));
    }
    return {
        sub: sub,
        pub: pub,
        send: send
    };
}

module.exports = {
    gearman: gearman,
    mongodb: mongodb,
    mysql: mysql,
    odm: odm,
    odmSync: odmSync,
    orm: orm,
    ormSync: ormSync,
    event: event
};