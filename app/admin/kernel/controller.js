module.exports = function (env, dir, cb) {
    var controllers = {},
            environment = {},
            callback,
            actions = {},
            fs = require('fs');


    function init(env, dir, cb) {
        environment = env;
        environment.controllers = controllers;
        environment.actions = actions;
        environment.controller = {get: get, run: run};
        environment.dir = dir;
        if (cb !== undefined && typeof cb === 'function') {
            callback = cb;
        }
    }
    init(env, dir, cb);

    function handler(controller, method, req, res, next) {
        if (callback === undefined || callback({
            res: res,
            req: req,
            next: next,
            controller: controller,
            method: method,
            callback: function () {
                controller[method](req, res, next);
            }

        }) === false) {
            if (controller[method] == undefined) {
                next();
                return;
            }
            controller[method](req, res, next);
        }
    }
    function wrapper(name, method, req, res, next) {
        var arr, name, path, controller, method;
        if (controllers[name] === undefined) {
            path = dir + name + '.js';
            fs.exists(path, function (exists) {
                if (exists) {
                    controller = controllers[name] = require(path);
                    if (controller[method] === undefined) {
                        console.warn('not found method:%s controller.run: %s path->%s', method, req.query.action, path);
                    }
                    if (controller.init !== undefined) {
                        controller.init(environment, function () {
                            handler(controller, method, req, res, next);
                        });
                    } else {
                        handler(controller, method, req, res, next);
                    }
                } else {
                    console.warn('not found controller.run: %s path->%s', req.query.action, path);
                    next();
                }
            });
        } else {
            controller = controllers[name];
            if (controller[method] === undefined) {
                console.warn('not found method:%s controller.run: %s path->%s', method, req.query.action, path);
            }
            handler(controller, method, req, res, next);
        }
    }

    function run(req, res, next) {
        var arr, name, path, controller, method;
        if (actions[req.query.action] === undefined) {
            arr = req.query.action.split('.');
            if (arr.length <= 1) {
                actions[req.query.action] = null;
                next();
                return;
            }
            method = arr.splice(-1, 1)[0];
            name = arr.join('.');
        } else if (actions[req.query.action] === null) {
            next();
            return;
        } else {
            actions[req.query.action] = name;
        }
        req.params.action = {
            controller: name,
            method: method
        };
        if (controllers[name] === undefined) {
            path = dir + name + '.js';
            fs.exists(path, function (exists) {
                if (exists) {
                    controller = controllers[name] = require(path);
                    if (controller[method] === undefined) {
                        console.warn('not found method:%s controller.run: %s path->%s', method, req.query.action, path);
                    }
                    if (controller.init !== undefined) {
                        controller.init(environment, function () {
                            handler(controller, method, req, res, next);
                        });
                    } else {
                        handler(controller, method, req, res, next);
                    }
                } else {
                    console.warn('not found controller.run: %s path->%s', req.query.action, path);
                    next();
                }
            });
        } else {
            controller = controllers[name];
            if (controller[method] === undefined) {
                console.warn('not found method:%s controller.run: %s path->%s', method, req.query.action, path);
            }
            handler(controller, method, req, res, next);
        }

    }
    function get(name, cb) {
        var controller, path;
        if (controllers[name] === undefined) {
            path = dir + name + '.js';
            fs.exists(path, function (exists) {
                if (exists) {
                    controller = controllers[name] = require(path);
                    if (controller.init !== undefined) {
                        controller.init(environment);
                    }
                    return cb(controller);
                } else {
                    console.warn('not found controller.get: %s path->%s', name, path);
                }
            });
        } else {
            return cb(controllers[name]);
        }
    }
    return{
        init: init,
        env: environment,
        get: get,
        handler: wrapper,
        run: run
    };
};