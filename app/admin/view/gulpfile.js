var gulp = require('gulp'),
        jade = require('gulp-jade'),
        bower = require('gulp-bower'),
        clean = require('gulp-clean'),
        connect = require('gulp-connect');
var root = __dirname + '/../www/', view = __dirname;
gulp.task('connect', function () {
    connect.server({
        root: root,
        port: 8083,
        livereload: true
    });
});
gulp.task('bower-install', function () {
    return bower({cmd: 'install'});
});
gulp.task('bower-update', function () {
    return bower({cmd: 'update'});
});
gulp.task('clean', function () {
    gulp.src(root + '/js/', {read: false}).pipe(clean({force: true}));
    gulp.src(root + '/css/', {read: false}).pipe(clean({force: true}));
    gulp.src(root + '/fonts/', {read: false}).pipe(clean({force: true}));
    gulp.src(root + '/img/', {read: false}).pipe(clean({force: true}));
});
gulp.task('bower-component', function () {
//    var bower = require('path').normalize(__dirname + '/bower_components');
    var bower = './bower_components';
    var jquery = bower + '/jquery/dist';
    gulp.src(jquery + '/*.min.js').pipe(gulp.dest(root + '/js/'));
    var bootstrap = bower + '/bootstrap/dist';
    gulp.src(bootstrap + '/js/*.min.js').pipe(gulp.dest(root + '/js/'));
    gulp.src(bootstrap + '/css/*.{min.css,map}').pipe(gulp.dest(root + '/css/'));
    gulp.src(bootstrap + '/fonts/*.*').pipe(gulp.dest(root + '/fonts/'));
    var blueimpBootstap = bower + '/blueimp-bootstrap-image-gallery';
    gulp.src(blueimpBootstap + '/js/*.min.js').pipe(gulp.dest(root + '/js/'));
    gulp.src(blueimpBootstap + '/css/*.{min.css,map}').pipe(gulp.dest(root + '/css/'));
    gulp.src(blueimpBootstap + '/fonts/*.*').pipe(gulp.dest(root + '/fonts/'));
    var blueimp = bower + '/blueimp-gallery';
    gulp.src(blueimp + '/js/*.min.js').pipe(gulp.dest(root + '/js/'));
    gulp.src(blueimp + '/js/blueimp-gallery-fullscreen.js').pipe(gulp.dest(root + '/js/'));
    gulp.src(blueimp + '/js/blueimp-helper.js').pipe(gulp.dest(root + '/js/'));
    gulp.src(blueimp + '/js/blueimp-gallery-indicator.js').pipe(gulp.dest(root + '/js/'));
    gulp.src(blueimp + '/css/*.{min.css,map}').pipe(gulp.dest(root + '/css/'));
    gulp.src(blueimp + '/css/blueimp-gallery-indicator.css').pipe(gulp.dest(root + '/css/'));
    gulp.src(blueimp + '/fonts/*.*').pipe(gulp.dest(root + '/fonts/'));
    var bootstrapTable = bower + '/bootstrap-table/dist';
    gulp.src(bootstrapTable + '/*.{min.js,js}').pipe(gulp.dest(root + '/js/'));
    gulp.src(bootstrapTable + '/*.{min.css,map}').pipe(gulp.dest(root + '/css/'));
    var bootstrapFileinput = bower + '/bootstrap-fileinput';
    gulp.src(bootstrapFileinput + '/js/*.min.js').pipe(gulp.dest(root + '/js/'));
    gulp.src(bootstrapFileinput + '/css/*.{css,map}').pipe(gulp.dest(root + '/css/'));
    gulp.src(bootstrapFileinput + '/img/*.*').pipe(gulp.dest(root + '/img/'));
    var fuelux = bower + '/fuelux/dist';
    gulp.src(fuelux + '/js/*.min.js').pipe(gulp.dest(root + '/js/'));
    gulp.src(fuelux + '/css/*.{min.css,css,map}').pipe(gulp.dest(root + '/css/'));
    gulp.src(fuelux + '/fonts/*.*').pipe(gulp.dest(root + '/fonts/'));
    gulp.src(bower + '/eventemitter2/lib/*.js').pipe(gulp.dest(root + '/js/'));
    var moment = bower + '/moment';
    gulp.src(moment + '/*.min.js').pipe(gulp.dest(root + '/js/'));
    var ddslick = bower + '/ddslick';
    gulp.src(ddslick + '/*.min.js').pipe(gulp.dest(root + '/js/'));
//    var browser = bower + '/browser-logos/', copy = require('gulp-copy2'), fs = require('fs');
//    fs.readdir(browser, function (err, result) {
//        if (err) {
//            throw err;
//        }
//        var paths = [], i;
//        for (i in result) {
//            var path = browser + result[i];
//            if (fs.statSync(path).isDirectory()) {
//                paths.push({src: path + '/*.png', dest: root + '/img/browser/'});
//            }
//        }
//        copy(paths);
//    });
    var bootstrapSwitch = bower + '/bootstrap-switch/dist';
    gulp.src(bootstrapSwitch + '/js/*.min.js').pipe(gulp.dest(root + '/js/'));
    gulp.src(bootstrapSwitch + '/css/bootstrap3/*.{min.css,map}').pipe(gulp.dest(root + '/css/'));
    var bootstrapTouchspin = bower + '/bootstrap-touchspin/dist';
    gulp.src(bootstrapTouchspin + '/*.min.js').pipe(gulp.dest(root + '/js/'));
    gulp.src(bootstrapTouchspin + '/*.{min.css,map}').pipe(gulp.dest(root + '/css/'));
    var bootstrapNavWizard = bower + '/bootstrap-nav-wizard';
    gulp.src(bootstrapNavWizard + '/*.{min.css,map}').pipe(gulp.dest(root + '/css/'));
});
gulp.task('reload', function () {
    gulp.src(root + '/*.html').pipe(connect.reload());
    gulp.src(root + '/css/*.css').pipe(connect.reload());
    gulp.src(root + '/js/*.js').pipe(connect.reload());
    gulp.src(root + '/img/*').pipe(connect.reload());
    gulp.src(root + '/fonts/*').pipe(connect.reload());
});
gulp.task('user', function () {
    gulp.src(view + '/css/*.css').pipe(gulp.dest(root + '/css/'));
    gulp.src(view + '/img/flags/**/*').pipe(gulp.dest(root + '/img/flag'));
});
gulp.task('jade', function () {
    gulp.src(view + '/jade/index.jade').pipe(jade()).pipe(gulp.dest(root));
});

gulp.task('watch', function () {
    gulp.watch([root + '/css/*.css', root + '/*.html', root + '/js/*.js'], ['reload']);
    gulp.watch([view + '/css/*.css', view + '/img/flags/**/*'], ['user']);
    gulp.watch([view + '/jade/*.jade', view + '/jade/*/*.jade'], ['jade']);
});
//gulp.task('start', function () {
//    var app = require(app+'/admin.js');
//    app.listen(3000);
//});
//gulp.task('restart',function(){
//    require('opn')('http://localhost:3000');
//});
gulp.task('default', ['user', 'jade', 'watch', 'connect']);
gulp.task('install', ['bower-install', 'bower-component']);
gulp.task('setup', ['bower-component', 'user']);
gulp.task('update', ['bower-update', 'bower-component']);
