module.exports = {
    authorization: {
        modal: $(document.getElementById('modal-login')),
        form: $(document.getElementById('form-login')),
        alert: $(document.getElementById('alert-login')),
        action: $(document.getElementById('action-login'))
    },
    user: {
        modal: {
            edit: $(document.getElementById('modal-user-change')),
            remove: $(document.getElementById('modal-user-delete')),
            add: $(document.getElementById('modal-user-create')),
            removeAll: $(document.getElementById('modal-user-many-delete'))
        },
        table: {
            main: $(document.getElementById('table-user-main')),
            remove: $(document.getElementById('table-user-remove'))
        },
        user: $('select.user-select'),
        alert: $(document.getElementById('alert-user')),
        form: {
            edit: $(document.getElementById('form-user-change')),
            remove: $(document.getElementById('form-user-delete')),
            add: $(document.getElementById('form-user-create'))
        },
        action: {
            panel: $(document.getElementById('panel-user-action')),
            add: $(document.getElementById('action-user-create')),
            edit: $(document.getElementById('action-user-change')),
            remove: {
                one: $(document.getElementById('action-user-delete')),
                many: $(document.getElementById('action-user-many-delete'))
            }
        }
    },
    account:{

        modal: {
            edit: $(document.getElementById('modal-account-change')),
            remove: $(document.getElementById('modal-account-delete')),
            add: $(document.getElementById('modal-account-create')),
            removeAll: $(document.getElementById('modal-account-many-delete'))
        },
        table: {
            main:$(document.getElementById('table-account-main')),
            remove:$(document.getElementById('table-account-remove'))
        },
        alert: $(document.getElementById('alert-account')),
        form: {
            edit: $(document.getElementById('form-account-change')),
            remove: $(document.getElementById('form-account-delete')),
            add: $(document.getElementById('form-account-create'))
        },
        action: {
            add: $(document.getElementById('action-account-create')),
            edit: $(document.getElementById('action-account-change')),
            remove: {
                one: $(document.getElementById('action-account-delete')),
                many: $(document.getElementById('action-account-many-delete'))
            }
        }
    },
    cron:{

        modal: {
            edit: $(document.getElementById('modal-cron-change')),
            remove: $(document.getElementById('modal-cron-delete')),
            add: $(document.getElementById('modal-cron-create')),
            removeAll: $(document.getElementById('modal-cron-many-delete'))
        },
        table: {
            main:$(document.getElementById('table-cron-main')),
            remove:$(document.getElementById('table-cron-remove'))
        },
        alert: $(document.getElementById('alert-cron')),
        form: {
            edit: $(document.getElementById('form-cron-change')),
            remove: $(document.getElementById('form-cron-delete')),
            add: $(document.getElementById('form-cron-create'))
        },
        action: {
            add: $(document.getElementById('action-cron-create')),
            edit: $(document.getElementById('action-cron-change')),
            remove: {
                one: $(document.getElementById('action-cron-delete')),
                many: $(document.getElementById('action-cron-many-delete'))
            }
        }
    },
    task:{
        form:$(document.getElementById('form-account-task')),
        upload: $(document.getElementById('task-img'))
    }
};