var
        cfg = require('cfg'),
        action = cfg.action,
        document = require('dom'),
        event = require('event'),
        db = require('db'),
        func = require('func'),
        msg = require('msg');
request = require('request').action;
module.exports = {
    "authorization.logout": function () {
        $.ajax({url: action('authorization.logout')}).done(function (data, textStatus, jqXHR) {
            db.auth = false;
            db.user = {};
            event.emit('data.auth', {});
            location.reload(true);
        });
    },
    "authorization.login": function () {
        var dom = document.authorization;
        $.ajax({
            url: action('authorization.login'),
            data: dom.form.serializeArray(),
            method: 'post'
        }).done(function (data, textStatus, jqXHR) {
            if (jqXHR.status === 200 && data.result) {
                db.auth = true;
                db.user = data.result;
                event.emit('data.auth', data);
                dom.modal.modal('hide');
            } else {
                db.auth = false;
                dom.modal.modal('show');
            }
        });
    },
    "authorization.is": function () {
        var dom = document.authorization;
        $.ajax({url: action('authorization.is')}).done(function (data, textStatus, jqXHR) {
            if (data.result) {
                db.auth = true;
                db.user = data.result;
                event.emit('data.auth', data);
                dom.modal.modal('hide');
            } else {
                db.auth = false;
                dom.modal.modal('show');
            }
        });
    },
    "user.create": function () {
        var dom = document.user;
        request('user.create', dom.form.add.serializeArray(), function (data) {
            if (data.result) {
                event.emit('refresh.user', 'add', data.result);
                dom.modal.add.modal('hide');
                dom.form.add.find('input[type="text"]').val('');
            } else {
                console.warn(data);
            }
        });
    },
    "user.change": function () {
        var dom = document.user;
        request('user.change', dom.form.edit.serializeArray(), function (data) {
            if (data.result) {
                event.emit('refresh.user', 'edit', data.result);
                dom.modal.edit.modal('hide');
            } else {
                console.warn(data);
            }
        });
    },
    "user.remove": function () {
        var dom = document.user;
        var id = dom.form.remove.find('input[name="id"]').val();
        request('user.remove', {id: id}, function (data) {
            if (data.result) {
                event.emit('refresh.user', 'remove', [parseInt(id)]);
                dom.modal.remove.modal('hide');
            } else {
                console.warn(data);
            }
        });
    },
    "user.remove_all": function () {
        var dom = document.user;
        var data = dom.table.main.bootstrapTable('getSelections');
        if (data.length === 0) {
            return;
        }
        var result = $.map(data, function (row) {
            return row.id;
        });
        request('user.remove_all', {id: result}, function (data) {
            if (data.result) {
                event.emit('refresh.user', 'remove', result);
                dom.modal.removeAll.modal('hide');
            } else {
                console.warn(data);
            }
        });
    },
    "account.create":function(){
        var dom = document.account;
        request('account.create', dom.form.add.serializeArray(), function (data) {
            if (data.status === 'success') {
                event.emit('refresh.account', 'add', data.result);
                dom.modal.add.modal('hide');
                dom.form.add.find('input[type="text"]').val('');
            } else {
                console.warn(data);
            }
        });
    },
    "account.change": function () {
        var dom = document.account;
        request('account.change', dom.form.edit.serializeArray(), function (data) {
            if (data.result) {
                event.emit('refresh.account', 'edit', data.result);
                dom.modal.edit.modal('hide');
            } else {
                console.warn(data);
            }
        });
    },
    "account.remove": function () {
        var dom = document.account;
        var id = dom.form.remove.find('input[name="_id"]').val();
        request('account.remove', {_id: id}, function (data) {
            if (data.result) {
                event.emit('refresh.account', 'remove', [id]);
                dom.modal.remove.modal('hide');
            } else {
                console.warn(data);
            }
        });
    },
    "account.remove_all": function () {
        var dom = document.account;
        var data = dom.table.main.bootstrapTable('getSelections');
        if (data.length === 0) {
            return;
        }
        var result = $.map(data, function (row) {
            return row._id;
        });
        request('account.remove_all', {_id: result}, function (data) {
            if (data.result) {
                event.emit('refresh.account', 'remove', result);
                dom.modal.removeAll.modal('hide');
            } else {
                console.warn(data);
            }
        });
    },
    "task.create":function(){
        var dom = document.task;
        request('task.create', dom.form.serializeArray(), function (data) {
            if (data.status === 'success') {
                event.emit('refresh.task', 'add', data.result);
                dom.modal.add.modal('hide');
                dom.form.add.find('input[type="text"]').val('');
            } else {
                console.warn(data);
            }
        });
    },
    "banner.remove": function () {
        var wrapper = $(this), id = wrapper.data('id');
        request('banner.remove', {id: id}, function (data) {
            if (data.result) {
                wrapper.parent().parent().remove();
                delete db.banner.data[id];
                var data = db.banner.data, index = {};
                for (var id in data) {
                    var v = data[id];
                    if (index[v.uid] === undefined) {
                        index[v.uid] = [];
                    }
                    index[v.uid].push(v.id);
                }
                db.banner.index = index;
                event.emit('data.banner');
            } else {
                alert(dom.alert, 'alert-warning', data.error);
                console.warn(data.error);
            }
        });
    },
    "banner.list": function () {
        request('banner.list', function (data) {
            if (data.result) {
                var k, v, result = {}, index = {},ids ={}, down = cfg.server.down, users = db.users.index;
                for (k in data.result) {
                    v = data.result[k];
                    if (v.url) {
                        v.src = v.link;
                    } else {
                        v.src = down + v.link;
                    }
                    v.user = users[v.uid];
                    result[v.id] = v;
                    ids[v._id]= v.id;
                    if (index[v.uid] === undefined) {
                        index[v.uid] = [];
                    }
                    index[v.uid].push(v.id);
                }
                db.banner = {data: result, index: index,ids:ids};
                event.emit('data.banner', db.banner);
            } else {
                console.warn(data.error);
            }
        });
    }

};