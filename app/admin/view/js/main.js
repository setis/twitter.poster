$.extend($.fn.bootstrapTable.defaults, $.fn.bootstrapTable.locales['ru-RU']);
$.fn.bootstrapSwitch.defaults.size = 'small';
$.ajaxSetup({
    type: "GET",
    xhrFields: {withCredentials: true},
//    crossDomain: true,
    global: true,
    dataType: 'json',
    cache: false,
//    error: function (jqXHR, textStatus, errorThrown) {
//        if (jqXHR.status == 403) {
//            console.warn("no access");
//        }
//    }
});
moment.locale("ru");
window.modal = require('modal');
var event = require('event');
require('auth')();
require('user')();
require('account')();
require('task')();
require('cron')();
require('job')();
// require('proxy')();
$(document).ready(function () {
    require('dom');
    setTimeout(function () {
        event.emit('app.*');
    }, 750);
});
