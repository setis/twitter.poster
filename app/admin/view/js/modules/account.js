/**
 * Created by alex on 30.04.16.
 */
var func = require('func'),
    event = require('event'),
    cfg = require('cfg'),
    action_index,
    change = false,
    db = require('db'),
    setting = cfg.modules.user;
db.account = {
    index: {},
    data: []
};
function handler(data) {
    db.account = {index: {}, data: data};
    for (var k in data) {
        var row = data[k];
        db.account.index[row._id] = row.login;
    }
    event.emit('data.account', db.account.index);
}


function init() {
    var dom = require('dom').account;
    var configure = {
        method: 'GET',
        url: cfg.action('account.list'),
        toggle: "table",
        cache: false,
        striped: true,
        search: true,
        pagination: true,
        pageSize: 50,
        pageList: [10, 25, 50, 100, "ALL"],
        sidePagination: "client",
        ajaxOptions: {
            xhrFields: {withCredentials: true},
            crossDomain: true
        },
        queryParams: function (params) {
            return {in: params};
        },
//        ajax:function(params){
////            https://github.com/wenzhixin/bootstrap-table-examples/blob/master/options/custom-ajax.html
//        },
        responseHandler: function (res) {
            if (res.result) {
                return res.result;
            }
        },
        toolbar: '#toolbar-account',
        undefinedText: '',
        silent: true,
        idField: 'code',
        sortName: 'code',
        sortOrder: 'asc',
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
//    selectItemName: "toolbar-genre",
        columns: [
            {
                field: 'state',
                checkbox: true
            },
            {
                field: 'login',
                title: 'Login',
                align: 'left',
                valign: 'top',
                sortable: true
            }, {
                field: 'password',
                title: 'Password',
                align: 'left',
                valign: 'top',
                sortable: true
            }, {
                field: 'ip',
                title: 'ip',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value, row) {
                    if (row.country) {
                        return value + ' <img src="/img/flag/16/' + row.country.toLowerCase() + '.png"> ' + moment(row.time).fromNow();
                    }
                    return value + ' ' + moment(row.time).fromNow();

                }
            }, {
                field: 'info',
                title: 'status',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value, row) {
                    function status(row) {
                        if (row.access_token_key && row.access_token_secret && row.info !== undefined && Object.keys(row.info).length) {
                            // if (row.access_token_key && row.access_token_secret && row.bitly) {
                            //     if (row.info !== undefined && Object.keys(row.info).length) {
                            return 5;
                            // }
                        }
                        if (row.error !== undefined && row.error.msg === 'login and pasword') {
                            return 1;
                        }
                        if (row.error !== undefined && row.error.msg === 'confirmation code') {
                            return 2;
                        }
                        return 0;
                    }

                    console.log(status(row), row.error);
                    var _class, msg,add ='';
                    switch (status(row)) {
                        default:
                        case 0:
                            _class = 'text-warning';
                            msg = 'получение api';
                            if(row.error !== undefined && row.error.msg !== undefined){
                                add = '<span class="text-danger">'+ row.error.msg+'</span>';
                            }

                            break;
                        case 1:
                            _class = 'text-danger';
                            msg = 'не вверен логин или пароль';
                            break;
                        case 2:
                            _class = 'text-danger';
                            msg = 'требуется подверждение';
                            break;
                        case 5:
                            _class = 'text-success';
                            msg = 'готово';
                            break;
                    }
                    return '<p class="' + _class + '">' + msg + '</p>'+add;
                }
            }, {
                field: 'operate',
                title: 'Действия',
                align: 'center',
                valign: 'middle',
                clickToSelect: false,
                formatter: function (value, row, index) {
                    return '<a class="edit ml10" href="javascript:void(0)" title="Редактировать">' +
                        '<i class="glyphicon glyphicon-edit"></i>' +
                        '</a>' +
                        '<a class="remove ml10" href="javascript:void(0)" title="Удалить">' +
                        '<i class="glyphicon glyphicon-remove"></i>' +
                        '</a>';
                },
                events: {
                    'click .edit': function (e, value, row, index) {
                        var modal = dom.modal.edit;
                        modal.modal('show');
                        modal.find('[name="login"]').val(row.login);
                        modal.find('[name="password"]').val(row.password);
                        modal.find('[name="ip"]').val(row.last.ip);
                        action_index = index;
                    },
                    'click .remove': function (e, value, row, index) {
                        var modal = dom.modal.remove;
                        modal.modal('show');
                        modal.find('[name="_id"]').val(row._id);
                        modal.find('[name="login"]').val(row.login);
                        modal.find('[name="password"]').val(row.password);
                        modal.find('[name="ip"]').val(row.last.ip);
                        action_index = index;
                    }
                }
            }]
    };
    event.on('refresh.account', function (event, data) {
        change = true;
        func.refresh({
            cfg: setting.refreshTable,
            event: event,
            data: data,
            id: '_id',
            db: function () {
                db.account = {
                    index: {},
                    data: {}
                };
            },
            index: action_index,
            dom: dom.table.main
        });
    });

    dom.table.main.bootstrapTable(configure).on('pre-body.bs.table', function (e, data) {
        if (change) {
            change = false;
            handler(data);
        }
    }).on('load-success.bs.table', function (e, data) {
        handler(data);
    });
    var action = require('controller');

    dom.action.add.click(action['account.create']);
    dom.action.edit.click(action['account.change']);
    dom.action.remove.one.click(action['account.remove']);
    dom.action.remove.many.click(action['account.remove_all']);

    dom.table.remove.bootstrapTable({
        toggle: "table",
        strexploited: true,
        search: true,
        pagination: true,
        pageSize: 10,
        pageList: [10, 25, 50, 100, "ALL"],
        sidePagination: "client",
        undefinedText: '',
        silent: true,
        idField: 'code',
        sortName: 'code',
        sortOrder: 'asc',
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
        columns: [
            {
                field: 'login',
                title: 'Login',
                align: 'left',
                valign: 'top',
                sortable: true
            }, {
                field: 'password',
                title: 'Password',
                align: 'left',
                valign: 'top',
                sortable: true
            }, {
                field: 'ip',
                title: 'ip',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value, row) {
                    if (row.country) {
                        return value + ' <img src="/img/flag/16/' + row.country.toLowerCase() + '.png"> ' + moment(row.time).fromNow();
                    }
                    return value + ' ' + moment(row.time).fromNow();

                }
            }, {
                field: 'info',
                title: 'status',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value, row) {
                    function status(row) {
                        if (row.access_token_key && row.access_token_secret && row.bitly) {
                            return 1;
                        } else if (row.info && Object.keys(row.info).length) {
                            return 2;
                        } else {
                            return 0;
                        }
                    }

                    var _class, msg;
                    switch (status(row)) {
                        default:
                        case 0:
                            _class = 'text-danger';
                            msg = 'получение api';
                            break;
                        case 1:
                            _class = 'text-warning';
                            msg = 'получение данных';
                            break;
                        case 2:
                            _class = 'text-success';
                            msg = 'готово';
                            break;
                    }
                    return '<p class="' + _class + '">' + msg + '</p>';
                }
            }
        ]
    });
    dom.modal.removeAll.on('shown.bs.modal', function () {
        dom.table.remove
            .bootstrapTable('load', dom.table.main.bootstrapTable('getSelections'))
            .bootstrapTable('resetView');
    });
    var upload = {
        uploadUrl: require('cfg').server.import,
        uploadAsync: true,
        minFileCount: 1,
        maxFileCount: 1,
        // overwriteInitial: false,
        actions: '',
        language: 'ru',
        showCaption: false,
        showPreview: false,
        showRemove: true,
        showUpload: true,
        showCancel: false,
        showUploadedThumbs: false

    };
    $('#file-account-import').fileinput(upload);
    $('#file-account-import')
        .on('fileuploaded', function (event, data, previewId, index) {
            dom.table.main.bootstrapTable('refresh');
            $('#modal-account-import').modal('hide');
        });
}


module.exports = function () {
    event.on('app.account', init);
};