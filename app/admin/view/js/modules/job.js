/**
 * Created by alex on 30.04.16.
 */
var func = require('func'),
    event = require('event'),
    cfg = require('cfg'),
    action_index,
    change = false,
    db = require('db'),
    setting = cfg.modules.user;
dom = false;
db.job = {
    index: {},
    data: []
};
function handler(data) {
    db.job = {index: {}, data: data};
    for (var k in data) {
        var row = data[k];
        db.job.index[row._id] = row.login;
    }
    event.emit('data.job', db.job.index);
}

function initDom() {
    dom = {
        table: {
            main: $('#table-job-main')
        }
    };
}
function init() {
    initDom();
    var configure = {
        method: 'GET',
        url: cfg.action('task.status'),
        toggle: "table",
        cache: false,
        striped: true,
        search: true,
        pagination: true,
        pageSize: 100,
        pageList: [10, 25, 50, 100, 0],
        sidePagination: "client",
        ajaxOptions: {
            xhrFields: {withCredentials: true},
            crossDomain: true
        },
        queryParams: function (params) {
            return {in: params};
        },
        responseHandler: function (res) {
            if (res.result) {
                return res.result;
            }
        },
        toolbar: '#toolbar-job',
        undefinedText: '',
        silent: true,
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,

        columns: [
            {
                field: 'state',
                checkbox: true
            },
            {
                field: 'result',
                title: 'status',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value, row) {
                    return "<span class='text-success'>" + value.success + "</span>/" +
                        "<span class='text-danger'>" + value.fail + "</span>/" +
                        "<span class='text-info'>(" + value.count + ")</span> ";
                    // "- " +
                    // "<span class='text-muted'>" + row.data.filter.count + "</span>"
                }
            }, {
                field: 'data',
                title: 'name',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value, row) {
                    return value.name + " " +
                        "img: <span class='text-info'>"+value.data.img.length+"</span>" +
                        " msg: <span class='text-muted'>" + value.data.msg + "</span>";

                }
            }, {
                field: 'data',
                title: 'country',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value, row) {
                    return value.filter.country + ' <img src="/img/flag/16/' + value.filter.country.toLowerCase() + '.png">';

                }
            }, {
                field: 'data',
                title: 'lang',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value, row) {
                    return value.filter.lang;
                }
            }, {
                field: 'date',
                title: 'date',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value, row) {
                    return moment(value).fromNow();

                }
            }, {
                field: 'operate',
                title: 'Действия',
                align: 'center',
                valign: 'middle',
                clickToSelect: false,
                formatter: function (value, row, index) {
                    return '<a class="edit ml10" href="javascript:void(0)" title="Редактировать">' +
                        '<i class="glyphicon glyphicon-edit"></i>' +
                        '</a>' +
                        '<a class="remove ml10" href="javascript:void(0)" title="Удалить">' +
                        '<i class="glyphicon glyphicon-remove"></i>' +
                        '</a>';
                },
                events: {
                    'click .edit': function (e, value, row, index) {
                        var modal = dom.modal.edit;
                        modal.modal('show');
                        modal.find('[name="login"]').val(row.login);
                        modal.find('[name="password"]').val(row.password);
                        modal.find('[name="ip"]').val(row.last.ip);
                        action_index = index;
                    },
                    'click .remove': function (e, value, row, index) {
                        var modal = dom.modal.remove;
                        modal.modal('show');
                        modal.find('[name="_id"]').val(row._id);
                        modal.find('[name="login"]').val(row.login);
                        modal.find('[name="password"]').val(row.password);
                        modal.find('[name="ip"]').val(row.last.ip);
                        action_index = index;
                    }
                }
            }]
    };
    event.on('refresh.job', function (event, data) {
        change = true;
        func.refresh({
            cfg: setting.refreshTable,
            event: event,
            data: data,
            id: '_id',
            db: function () {
                db.job = {
                    index: {},
                    data: {}
                };
            },
            index: action_index,
            dom: dom.table.main
        });
    });

    dom.table.main.bootstrapTable(configure).on('pre-body.bs.table', function (e, data) {
        if (change) {
            change = false;
            handler(data);
        }
    }).on('load-success.bs.table', function (e, data) {
        handler(data);
    });
    // var action = require('controller');
    // dom.action.remove.one.click(action['job.remove']);
    // dom.action.remove.many.click(action['job.remove_all']);
    //
    // dom.table.remove.bootstrapTable({
    //     toggle: "table",
    //     strexploited: true,
    //     search: true,
    //     pagination: true,
    //     pageSize: 10,
    //     pageList: [10, 25, 50, 100, "ALL"],
    //     sidePagination: "client",
    //     undefinedText: '',
    //     silent: true,
    //     idField: 'code',
    //     sortName: 'code',
    //     sortOrder: 'asc',
    //     showColumns: true,
    //     showRefresh: true,
    //     showToggle: true,
    //     minimumCountColumns: 2,
    //     clickToSelect: true,
    //     columns: [
    //         {
    //             field: 'login',
    //             title: 'Login',
    //             align: 'left',
    //             valign: 'top',
    //             sortable: true
    //         }, {
    //             field: 'password',
    //             title: 'Password',
    //             align: 'left',
    //             valign: 'top',
    //             sortable: true
    //         }, {
    //             field: 'ip',
    //             title: 'ip',
    //             align: 'left',
    //             valign: 'top',
    //             sortable: true,
    //             formatter: function (value, row) {
    //                 if (row.country) {
    //                     return value + ' <img src="/img/flag/16/' + row.country.toLowerCase() + '.png"> ' + moment(row.time).fromNow();
    //                 }
    //                 return value + ' ' + moment(row.time).fromNow();
    //
    //             }
    //         }, {
    //             field: 'info',
    //             title: 'status',
    //             align: 'left',
    //             valign: 'top',
    //             sortable: true,
    //             formatter: function (value, row) {
    //                 function status(row) {
    //                     if (row.access_token_key && row.access_token_secret && row.bitly) {
    //                         return 1;
    //                     } else if (row.info && Object.keys(row.info).length) {
    //                         return 2;
    //                     } else {
    //                         return 0;
    //                     }
    //                 }
    //
    //                 var _class, msg;
    //                 switch (status(row)) {
    //                     default:
    //                     case 0:
    //                         _class = 'text-danger';
    //                         msg = 'получение api';
    //                         break;
    //                     case 1:
    //                         _class = 'text-warning';
    //                         msg = 'получение данных';
    //                         break;
    //                     case 2:
    //                         _class = 'text-success';
    //                         msg = 'готово';
    //                         break;
    //                 }
    //                 return '<p class="' + _class + '">' + msg + '</p>';
    //             }
    //         }
    //     ]
    // });
    // dom.modal.removeAll.on('shown.bs.modal', function () {
    //     dom.table.remove
    //         .bootstrapTable('load', dom.table.main.bootstrapTable('getSelections'))
    //         .bootstrapTable('resetView');
    // });
}


module.exports = function () {
    event.on('app.job', init);
};