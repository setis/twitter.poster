/**
 * Created by alex on 30.04.16.
 */
var func = require('func'),
    event = require('event'),
    cfg = require('cfg'),
    action_index,
    change = false,
    db = require('db');
db.cron = {
    index: {},
    data: []
};
function handler(data) {
    db.cron = {index: {}, data: data};
    for (var k in data) {
        var row = data[k];
        db.cron.index[row._id] = row;
    }
    event.emit('data.cron', db.cron.index);
}


function init() {
    var dom = require('dom').cron;
    var configure = {
        method: 'GET',
        url: cfg.action('cron.list'),
        toggle: "table",
        cache: false,
        striped: true,
        search: true,
        pagination: true,
        pageSize: 50,
        pageList: [10, 25, 50, 100, "ALL"],
        sidePagination: "client",
        ajaxOptions: {
            xhrFields: {withCredentials: true},
            crossDomain: true
        },
        queryParams: function (params) {
            return {in: params};
        },
//        ajax:function(params){
////            https://github.com/wenzhixin/bootstrap-table-examples/blob/master/options/custom-ajax.html
//        },
        responseHandler: function (res) {
            if (res.result) {
                return res.result;
            }
        },
        toolbar: '#toolbar-cron',
        undefinedText: '',
        silent: true,
        idField: 'code',
        sortName: 'code',
        sortOrder: 'asc',
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
        columns: [
            {
                field: 'state',
                checkbox: true
            },
            {
                field: 'name',
                title: 'name',
                align: 'left',
                valign: 'top',
                sortable: true
            }, {
                field: 'country',
                title: 'country',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value) {
                    return value + ' (<img src="/img/flag/16/' + value.toLowerCase() + '.png">)';
                }
            }, {
                field: 'lang',
                title: 'lang',
                align: 'left',
                valign: 'top',
                sortable: true
            }, {
                field: 'accounts',
                title: 'account',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value, row, index) {
                    return (value.length === 0) ? 'не фиксируемый(' + row.count + ')' : 'фиксируемый список(' + row.count + ')';
                }
            }, {
                field: 'limit',
                title: 'limit',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value, row, index) {
                    var begin = moment(value.begin).format("DD/MM/YYYY HH:mm");
                    var end = (value.end) ? 'до ' + moment(value.begin).format("DD/MM/YYYY HH:mm") : '';
                    return 'c ' + begin + ' ' + end;
                }
            }, {
                title: 'cron',
                align: 'left',
                valign: 'top',
                sortable: true,
                formatter: function (value, row, index) {
                    var msg = '';
                    switch (row.type) {
                        case 'date':
                            msg = "по дате";
                            var func;
                            if (row.time) {
                                msg += ' в ' + row.time + '<br>';
                                func = function (t) {
                                    return moment(t).format("DD/MM/YYYY");
                                };
                            } else {
                                func = function (t) {
                                    return moment(t).format("DD/MM/YYYY HH:mm");
                                };
                            }
                            msg += row.date.map(func).join('<br>');
                            break;
                        case 'weekly':
                            msg = "еженедельно в " + row.time;
                            msg += ' '+ row.weekly
                                .sort(function (a, b) {
                                    return a - b;
                                })
                                .map(function (v) {
                                    var msg;
                                    switch (v) {
                                        case 0:
                                        case 1:
                                            msg = 'пн';
                                            break;
                                        case 2:
                                            msg = 'вт';
                                            break;
                                        case 3:
                                            msg = 'ср';
                                            break;
                                        case 4:
                                            msg = 'чт';
                                            break;
                                        case 5:
                                            msg = 'пт';
                                            break;
                                        case 6:
                                            msg = 'сб';
                                            break
                                        case 7:
                                            msg = 'вс';
                                            break
                                    }
                                })
                                .join(',');
                            break;
                        case 'daily':
                            msg = "ежедневно в " + row.time;
                            break;
                    }
                    return msg;
                }
            }, {
                field: 'operate',
                title: 'Действия',
                align: 'center',
                valign: 'middle',
                clickToSelect: false,
                formatter: function (value, row, index) {
                    return '<a class="edit ml10" href="javascript:void(0)" title="Редактировать">' +
                        '<i class="glyphicon glyphicon-edit"></i>' +
                        '</a>' +
                        '<a class="remove ml10" href="javascript:void(0)" title="Удалить">' +
                        '<i class="glyphicon glyphicon-remove"></i>' +
                        '</a>';
                },
                events: {
                    'click .edit': function (e, value, row, index) {
                        var modal = dom.modal.edit;
                        modal.modal('show');
                        modal.find('[name="login"]').val(row.login);
                        modal.find('[name="password"]').val(row.password);
                        modal.find('[name="ip"]').val(row.last.ip);
                        action_index = index;
                    },
                    'click .remove': function (e, value, row, index) {
                        var modal = dom.modal.remove;
                        modal.modal('show');
                        modal.find('[name="login"]').val(row.login);
                        modal.find('[name="password"]').val(row.password);
                        modal.find('[name="ip"]').val(row.last.ip);
                        action_index = index;
                    }
                }
            }]
    };
    event.on('refresh.cron', function (event, data) {
        change = true;
        func.refresh({
            cfg: setting.refreshTable,
            event: event,
            data: data,
            db: function () {
                db.cron = {
                    index: {},
                    data: {}
                };
            },
            index: action_index,
            dom: dom.table.main
        });
    });

    dom.table.main.bootstrapTable(configure).on('pre-body.bs.table', function (e, data) {
        if (change) {
            change = false;
            handler(data);
        }
    }).on('load-success.bs.table', function (e, data) {
        handler(data);
    });
    var action = require('controller');

    dom.action.add.click(action['cron.create']);
    dom.action.edit.click(action['cron.change']);
    dom.action.remove.one.click(action['cron.remove']);
    dom.action.remove.many.click(action['cron.remove_all']);

    dom.table.remove.bootstrapTable({
        toggle: "table",
        strexploited: true,
        search: true,
        pagination: true,
        pageSize: 10,
        pageList: [10, 25, 50, 100, "ALL"],
        sidePagination: "client",
        undefinedText: '',
        silent: true,
        idField: 'code',
        sortName: 'code',
        sortOrder: 'asc',
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
        columns: [
            {
                field: 'name',
                title: 'name',
                align: 'left',
                valign: 'top',
                sortable: true
            }, {
                field: 'type',
                title: 'type',
                align: 'left',
                valign: 'top',
            }
        ]
    });
    dom.modal.removeAll.on('shown.bs.modal', function () {
        dom.table.remove
            .bootstrapTable('load', dom.table.main.bootstrapTable('getSelections'))
            .bootstrapTable('resetView');
    });
}

module.exports = function () {
    event.on('app.cron', init);
};