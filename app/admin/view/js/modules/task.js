/**
 * Created by alex on 30.04.16.
 */
var event = require('event'),
    cfg = require('cfg'),

    action = cfg.action;
var fixtime = false;
function init() {
    var dom = require('dom').task;
    dom.form.find("select[name='lang']").html((function () {
        var html = '', arr = require('data.lang');
        for (var lang in arr) {
            var name = arr[lang];
            html += '<option value="' + lang + '">' + name + '(' + lang + ')</option>';
        }
        return html;
    })());
    dom.form.wizard();
    dom.form.find("select[name='country']").html((function () {
        var list = require('data.country');
        var html = '';
        for (var i in list) {
            var v = list[i];
            html += '<option value="' + v.Code + '"  data-img-src="/img/flag/16/' + v.Code.toLowerCase() + '.png">' + v.Name + '(' + v.Code + ')</option>';
        }
        return html;
    })());
    $(dom.form.find('select')[0]).chosen({
        width: "100%",
    });
    dom.form.find('.datetime').datetimepicker({
        locale: 'ru'

    });
    dom.form.find('.date').datetimepicker({
        locale: 'ru',
        format: 'DD/MM/YYYY'

    });
    dom.form.find('.time').datetimepicker({
        locale: 'ru',
        format: 'HH:mm'

    });
    dom.form.find('input[type="checkbox"]').each(function () {
        var o = $(this);
        if (!o.hasClass('event-weekly')) {
            o.bootstrapSwitch();
        }
    });
    dom.form.find("select[name='type']").on('change', function () {
        var arr = dom.form.find('.task-repetition-type'), func;
        if ($(this).val() === 'repetition') {
            func = function () {
                $(this).show();
            };
        } else {
            func = function () {
                $(this).hide();
            };
        }
        arr.each(func);
    });
    dom.form.find("select[name='repetition_type']").on('change', function () {
        var val = $(this).val(), arr = dom.form.find('.task-repetition-type-select'), func;
        arr.each(function () {
            $(this).hide();
        });
        dom.form.find('.task-repetition-type-' + val).show();
    });
    dom.form.find("input[name='limit']").on('switchChange.bootstrapSwitch', function (event, state) {
        var arr = dom.form.find('.task-limit'), func;
        if (state) {
            func = function () {
                $(this).show();
            };
        } else {
            func = function () {
                $(this).hide();
            };
        }
        arr.each(func);
    });
    dom.form.find("input[name='fix']").on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            dom.form.wizard('addSteps', 4, [
                {
                    badge: '4',
                    label: 'account(аккаунты)',
                    pane: '<div class="form-group"><label>Список Аккаунтов</label></div><div class="form-group"><div id="alert-account-list"></div><div id="toolbar-account-list"></div></div>'
                }
            ]);
        } else {
            dom.form.wizard('removeSteps', 4, 1);
        }
    });
    dom.form.find("input[name='fixtime']").on('switchChange.bootstrapSwitch', function (event, state) {
        var o = dom.form.find('.task-fixtime');
        fixtime = state;
        var cfg = (fixtime) ? {
            locale: 'ru',
            format: 'DD/MM/YYYY'
        } : {
            locale: 'ru'
        };
        if (state) {
            o.show();
        } else {
            o.hide();
        }
        $('#task-date').find('.input-group').datetimepicker(cfg);
    });
    $('#task-action-date').on('click', function () {
        var el = document.createElement("div");
        el.innerHTML = '<div class="input-group"><div class="input-group-btn"><button type="button" class="btn btn-danger">delete</button></div><input type="text" name="repetition_type_date[]" class="form-control"><span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span></div>';
        var cfg = (fixtime) ? {
            locale: 'ru',
            format: 'DD/MM/YYYY'
        } : {
            locale: 'ru'
        };
        $(el).find('.input-group').datetimepicker(cfg);
        $(el).find('button').on('click', function () {
            $(el).remove();
        });
        $('#task-date').append(el);

    });

    var upload = {
        uploadUrl: require('cfg').server.up,
        uploadAsync: true,
        minFileCount: 1,
        maxFileCount: 4,
        // overwriteInitial: false,
        actions: '',
        language: 'ru',
        showCaption: true,
        showPreview: true,
        showRemove: true,
        showUpload: true,
        showCancel: true,
        showUploadedThumbs: false,
        allowedFileExtensions: ['jpg', 'gif', 'png', 'jpeg'],

    };
    dom.upload.fileinput(upload);
    dom.upload
        .on('fileuploaded', function (event, data, previewId, index) {
            console.log(data.form, previewId, index);
            if (data.response.status === 'success') {
                $('#task-img-list').append('<input type="hidden" name="image[]" value="' + data.response.result + '">');
            }
        })
        .on('fileclear', function () {
            // console.log('fileclear', arguments);
            $('#task-img-list').find('input').each(function () {
                var obj = $(this);
                $.ajax({
                    url: action('task.clear'),
                    data: {file: obj.val()},
                });
                obj.remove();
            });
        });
    dom.form.on('finished.fu.wizard', function () {
        console.log(dom.form.serializeArray());
        $.ajax({
            url: action('task.create'),
            data: dom.form.serializeArray(),
            success: function (data) {
                console.log(data);
                if (data.status === 'success') {
                    event.emit('refresh.task', 'add', data.result);
                    $('#task-img-list').find('input').each(function () {
                        $(this).remove();
                    });
                    clear();
                } else {
                    console.warn(data);
                }
            },
            error: function (obj) {
                var data = obj.responseJSON;
                $('#task-alert').html(
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' + 
                    '</button>'
                );
                alert($('#task-alert'), 'danger', data.error, 5e3);
            }
        });
    });
    $('#task-clear').on('click', clear);
}
function clear() {
    var dom = require('dom').task;
    dom.form.find("input[name='fixtime']").bootstrapSwitch('state', false, false);
    dom.form.find("input[name='limit']").bootstrapSwitch('state', false, false);
    dom.form.find("input[name='fix']").bootstrapSwitch('state', false, false);
    dom.upload.fileinput('clear');
    dom.form.find("select[name='country']").val('RU');
    dom.form.find("select[name='lang']").val('ru');
    dom.form.find('input[type="text"]').val('');
    dom.form.find('textarea').val('');
    dom.form.find("select[name='repetition_type']").val('daily');
    dom.form.find("select[name='type']").val('singel');
    $('#task-date').html('');
    dom.form.wizard('selectedItem', {
        step: 1
    });
}
module.exports = function () {
    event.on('app.task', init);
};