var async = require('async');
function add(req, res, next) {
    var data = {
        source: 'my-list',
        port: req.query.port,
        ip: req.query.ip,
        
    };
    req.mongoose.proxy.create(data, function (err, result) {
        if (err) {
            res.status(404).json({error: err, action: req.action, status: 'error'});
            return;
        }
        res.json({result: result, action: req.action, status: 'success'});
    });
}
function addList(req, res, next) {
    var arr = req.body.list
        .split("\n")
        .filter(function(v){
            return /(.*?):(\d+)/m.test(v);
        })
        .map(function (v) {
            var match = /(.*?):(\d+)/.exec(v.trim());
            return {
                source: 'my-list',
                port: Number(match[2]),
                ip: match[1].trim()
            }
        });
    if(arr.length === 0){
        res.json({result: [], action: req.action, status: 'success'});
        return;
    }
    var model = req.mongoose.proxy;
    async.map(
        arr,
        function (data, callback) {
            model.create(data, function (err, result) {
                if(err && err.code !== 11000){
                    console.log(err);
                    callback(err);
                    return;
                }
                callback(null,result);
            });
        },
        function (error, results) {
            if (error) {
                res.status(404).json({error: error, action: req.action, status: 'error'});
                return;
            }
            res.json({result: results.filter(function(v){return v !== null && v !== undefined;}), action: req.action, status: 'success'});
        }
    );
}
function view(req, res, next) {
    req.mongoose.proxy.find({}, function (err, result) {
        if (err) {
            res.status(404).json({error: err, action: req.action});
            return;
        }
        res.json({result: result, action: req.action});
    });
}
var ObjectId = require('mongoose').Types.ObjectId;
function remove(req, res, next) {
    req.mongoose.proxy.remove({_id: ObjectId(req.query._id)}, function (err) {
        if (err) {
            res.status(404).json({error: err, action: req.action});
            return;
        }
        res.json({result: true, action: req.action});
    });
}

function edit(req, res, next) {
    var data = {
        ip: req.query.ip,
        port: req.query.port,
        protocol: req.query.protocol
    };

    var model = req.mongoose.proxy;
    model.update({_id: ObjectId(req.query._id)}, {
        '$set': data
    }, function (err, result) {
        if (err) {
            res.status(404).json({error: err, action: req.action});
            return;
        }
        model.findOne({_id: ObjectId(req.query._id)}, function (err, result) {
            if (err) {
                res.status(404).json({error: err, action: req.action});
                return;
            }
            res.json({result: result, action: req.action});
        });

    });
}
function removeAll(req, res, next) {
    req.mongoose.proxy.remove({
        login: {
            '$in': req.query._id.map(function (id) {
                return ObjectId(id);
            })
        }
    }, function (err) {
        if (err) {
            res.status(404).json({error: err, action: req.action});
            return;
        }
        res.json({result: true, action: req.action});
    });
}
module.exports = {
    create: add,
    addList: addList,
    list: view,
    change: edit,
    remove: remove,
    remove_all: removeAll
};
