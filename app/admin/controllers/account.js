function add(req, res, next) {
    // if (req.session.root === false) {
    //     res.status(403).json({error: 'not access root', action: req.action});
    //     return;
    // }
    req.mongoose.account.create({
        login: req.query.login,
        password: req.query.password,
        ip: req.query.ip
    }, function (err, result) {
        if (err) {
            res.status(404).json({error: err, action: req.action, status: 'error'});
            return;
        }
        res.json({result: result, action: req.action, status: 'success'});
    });
}
function view(req, res, next) {
    req.mongoose.account.find({}, function (err, result) {
        if (err) {
            res.status(404).json({error: err, action: req.action});
            return;
        }
        res.json({result: result, action: req.action});
    });
}
var ObjectId = require('mongoose').Types.ObjectId;
function remove(req, res, next) {
    req.mongoose.account.remove({_id: ObjectId(req.query._id)}, function (err) {
        if (err) {
            res.status(404).json({error: err, action: req.action});
            return;
        }
        res.json({result: true, action: req.action});
    });
}
function search(req, res, next) {
    req.mongoose.account.find({'$or': [{id: '/.*' + req.query.search + '.*/i'}, {login: '/.*' + req.query.search + '.*/i'}]}, function (err, result) {
        if (err) {
            res.status(404).json({error: err, action: req.action});
            return;
        }
        res.json({result: result, action: req.action});
    });
}
function edit(req, res, next) {
    var data = {
        ip: req.query.ip,
        password: req.query.password
    };

    var model = req.mongoose.account;
    model.update({_id: ObjectId(req.query._id)}, {
        '$set': data
    }, function (err, result) {
        if (err) {
            res.status(404).json({error: err, action: req.action});
            return;
        }
        model.findOne({_id: ObjectId(req.query._id)}, function (err, result) {
            if (err) {
                res.status(404).json({error: err, action: req.action});
                return;
            }
            res.json({result: result, action: req.action});
        });

    });
}
function removeAll(req, res, next) {
    req.mongoose.account.remove({
        login: {
            '$in': req.query._id.map(function (id) {
                return ObjectId(id);
            })
        }
    }, function (err) {
        if (err) {
            res.status(404).json({error: err, action: req.action});
            return;
        }
        res.json({result: true, action: req.action});
    });
}
/**
 *
 * https://www.npmjs.com/package/fast-csv
 */
var csv = require("fast-csv"),
    fs = require('fs'),
    moment = require('moment'),
    random_useragent = require('random-useragent');
var headers = [
    "date",
    "ua",
    "u1",
    "login",
    "password",
    "link",
    "status",
    "u2",
    "u3",
    "u4",
    "ip"
];

function file(req, res, next) {
    function uaReplace(ua) {
        switch (ua) {
            case "Google Chrome":
                return "Chrome";
            case "Internet Explorer":
                return "IE";
            case "Mozilla FireFox":
                return "FireFox";
            default :
                return ua;
        }
    }
    var model = req.mongoose.account;
    var read = fs.createReadStream(req.file.path);
    var results = [], errors = [];
    csv
        .fromStream(read, {headers: headers, ignoreEmpty: true, delimiter: ";", escape: '"', quote: '"'})
        .on("data", function (data) {
            var name = uaReplace(data.ua);
            var ua;
            if(name !== null) {
                ua = random_useragent.getRandom(function (ua) {
                    return ua.osName === "Windows" && ua.browserName === name;
                });
            }else{
                ua = random_useragent.getRandom();
            }
            var create = {
                login: data.login,
                password: data.password,
                ip: data.ip,
                ua: ua,
                time: moment(data.date).toDate()

            };
            model
                .create(create, function (err, result) {
                    if (err) {
                        errors.push([create.login, err]);
                        return;
                    }
                    results.push(result);
                });

        })
        .on('error', function (err) {
            console.log(err);
        })

        .on("finish", function () {
            res.json({
                result: results,
                errors: errors,
                status: 'success',
                action: req.action
            });
            setTimeout(function () {
                fs.unlink(req.file.path);
            }, 5e3);
        });
}
module.exports = {
    file: file,
    create: add,
    list: view,
    change: edit,
    remove: remove,
    remove_all: removeAll,
    search: search
};
