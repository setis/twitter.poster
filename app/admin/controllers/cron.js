exports.list = function (req,res,next){
    req.mongoose.cron.find({},function(err,result){
        if (err) {
            res.status(500).json({
                error: err,
                data:data,
                status: 'error',
                action: {
                    controller: 'cron',
                    method: 'list'
                }
            });
            return;
        }
        res.json({
            result: result,
            status: 'success',
            action: {
                controller: 'cron',
                method: 'list'
            }
        });
    });
};