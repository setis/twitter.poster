/**
 * Created by alex on 05.05.16.
 */
var moment = require('moment'),
    fs = require('fs'),
    async = require('async'),
    ObjectId = require('mongoose').Types.ObjectId,
    uniqueRandomArray = require('unique-random-array'),
    dir = require('../../../config/admin.json').upload.dest;
function upload(req, res, next) {
    res.json({
        result: req.file.filename,
        status: 'success',
        action: {
            controller: 'task',
            method: 'upload'
        }
    });
}
function clear(req, res, next) {
    async.waterfall([
        function (callback) {
            var path = dir + '/' + req.query.file;
            fs.exists(path, function (status) {
                if (!status) {
                    callback(new Error('not found file'));
                    return;
                }
                callback(null, path);
            });
        },
        fs.unlink

    ], function () {
        res.json({
            status: 'success',
            action: {
                controller: 'task',
                method: 'clear'
            }
        });
    });

}

function task(req, res, next) {
    var img = [], accounts = [], count = Number(req.query.count) || 0;
    if (req.query.image && req.query.image.length) {
        img = req.query.image;
    }

    var data = {
        name: req.query.name,
        data: {
            msg: req.query.msg,
            img: img
        }
    };
    if (req.query.fix) {
        data.accounts = req.query.accounts.split(',');
    } else {
        data.accounts = [];
        data.filter = {
            country: req.query.country,
            lang: req.query.lang,
            count: count
        };
    }
    switch (req.query.type) {
        case 'singel':
        default:
            // console.log({
            //     date: new Date(),
            //     task: 'post.many',
            //     data: data
            // });
            req.mongoose.job.create({
                date: new Date(),
                task: 'post.many',
                data: data
            }, function (err, result) {
                if (err) {
                    res.status(500).json({
                        error: err,
                        status: 'error',
                        action: {
                            controller: 'task',
                            method: 'create'
                        }
                    });
                    return;
                }
                res.json({
                    result: result,
                    status: 'success',
                    action: {
                        controller: 'task',
                        method: 'create'
                    }
                });

            });
            break;
        case 'repetition':
            if (req.query.limit) {
                data.limit = {begin: new Date()};
                if (req.query.fixtime) {
                    data.limit.begin = moment(req.query.begin + ' ' + req.query.time, "DD/MM/YYYY HH:mm").toDate();
                    if (req.query.end) {
                        data.limit.end = moment(req.query.end + ' ' + req.query.time, "DD/MM/YYYY HH:mm").toDate();
                    }
                } else {
                    data.limit.begin = moment(req.query.begin, "DD/MM/YYYY HH:mm").toDate();
                    if (req.query.end) {
                        data.limit.end = moment(req.query.end, "DD/MM/YYYY HH:mm").toDate();
                    }
                }
            }
            switch (req.query.repetition_type) {
                case 'date':
                    data.type = 'date';
                    var func = (req.query.fixtime) ? function (t) {
                        console.log(t + ' ' + req.query.time, moment(t + ' ' + req.query.time, "DD/MM/YYYY HH:mm").toDate());
                        return moment(t + ' ' + req.query.time, "DD/MM/YYYY HH:mm").toDate();
                    } : function (t) {
                        return moment(t, "DD/MM/YYYY HH:mm").toDate();
                    }
                    if (req.query.fixtime) {
                        data.time = req.query.time;
                    }
                    data.date = req.query.repetition_type_date.map(func);
                    break;
                case 'daily':
                    data.type = 'daily';
                    data.time = req.query.time;
                    break;
                case 'weekly':
                    data.type = 'weekly';
                    data.weekly = req.query.repetition_type_weekly.map(function (v) {
                        return Number(v);
                    }).filter(function (v) {
                        return (v > 0 && v <= 7);
                    });
                    break;
            }
            req.mongoose.cron.create(data, function (err, result) {
                if (err) {
                    res.status(500).json({
                        error: err,
                        data: data,
                        status: 'error',
                        action: {
                            controller: 'task',
                            method: 'create'
                        }
                    });
                    return;
                }
                res.json({
                    result: result,
                    status: 'success',
                    action: {
                        controller: 'task',
                        method: 'create'
                    }
                });
            });
            break;
    }

}
function status(req, res, next) {
    async.waterfall(
        [
            function (callback) {
                req.mongoose.job.find({task: "post.many"}, callback);
            },
            function (jobs, callback) {
                async.map(
                    jobs,
                    function (data, callback) {
                        (function(job,callback){
                            job.result = {};
                            async.parallel(
                                {
                                    count: function (callback) {
                                        req.mongoose.job.count({
                                            task: 'post.once',
                                            source: {
                                                id: job._id,
                                                model: 'job'
                                            },
                                        }, callback);
                                    },
                                    success: function (callback) {
                                        req.mongoose.job.count({
                                            task: 'post.once',
                                            source: {
                                                id: job._id,
                                                model: 'job'
                                            },
                                            status: true
                                        }, callback);
                                    },
                                    fail: function (callback) {
                                        req.mongoose.job.count({
                                            task: 'post.once',
                                            source: {
                                                id: job._id,
                                                model: 'job'
                                            },
                                            status: false
                                        }, callback);
                                    }
                                }, function (error, result) {
                                    job.result = result;
                                    callback(error, job);
                                }
                            );
                        })(data._doc,callback);

                    },
                    callback
                );
            }
        ],
        function (error, result) {
            if (error) {
                res.status(500).json({
                    error: err,
                    status: 'error',
                    action: {
                        controller: 'task',
                        method: 'status'
                    }
                });
                return;
            }
            res.json({
                result: result,
                status: 'success',
                action: {
                    controller: 'task',
                    method: 'status'
                }
            });
        }
    );
}
function accounts(req, res, next) {
    Array.prototype.random = function (length) {
        return this[Math.floor((Math.random() * length))];
    }
    async.waterfall([
            function (callback) {
                req.mongoose.account.find({country: req.query.country, lang: req.query.lang}, '', function (error, result) {
                    if (error) {
                        callback(error);
                        return;
                    }
                    async.map(
                        result,
                        function (doc, callback) {
                            callback(null, doc._id);
                        },
                        callback
                    )
                });
            },
            function (accounts, callback) {
                if (accounts.length < 2) {
                    req.mongoose.account.find({_id: {$in: accounts}}, 'login time', callback);
                    return;
                }
                var count = Number(req.query.count) || 100;
                if (accounts.length < count) {
                    count = accounts.length;
                }
                var arr = [];
                for (var i = 0; i < count; i++) {
                    arr.push(i);
                }
                var rand = uniqueRandomArray(arr);
                var arr2 = []
                for (var i = 0; i < count; i++) {
                    arr2.push(accounts[rand()]);
                }
                req.mongoose.account.find({_id: {$in: arr2}}, 'login time', callback);
            }
        ],
        function (error, result) {
            if (error) {
                req.logger.error('task.accounts:', error);
                res.status(500).json({
                    error: err,
                    status: 'error',
                    action: {
                        controller: 'task',
                        method: 'accounts'
                    }
                });
                return;
            }
            res.json({
                result: result,
                status: 'success',
                action: {
                    controller: 'task',
                    method: 'accounts'
                }
            });
        });

}
exports.accounts = accounts;
exports.status = status;
exports.upload = upload;
exports.clear = clear;
exports.create = task;
