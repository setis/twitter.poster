/**
 * Created by alex on 28.06.16.
 */
var async = require('async'),
    ObjectId = require('mongoose').Types.ObjectId;
function add(req, res, next) {
    async.waterfall([
        function (callback) {
            req.mongoose.app.findOne({host: req.query.host.trim().toLowerCase()}, function (err, result) {
                if (err) {
                    req.logger.error(req.action, err);
                    callback(err);
                    return;
                }
                if (result) {
                    callback(new Error('not uniq host'));
                    return;
                }
                callback();
            });
        },
        function (callback) {
            switch (req.query.type) {
                default:
                case 'rand':
                    req.mongoose.account.find(
                        {
                            country: {$exists: true},
                            lang: {$exists: true},
                            access_token_key: {$exists: true},
                            access_token_secret: {$exists: true}
                        },
                        function (err, result) {
                            if (err) {
                                req.logger.error(err);
                                callback(err);
                                return;
                            }
                            if (result.length === 0) {
                                callback(new Error('not found accounts'));
                                return;
                            }
                            var random = require("random-js")(); // uses the nativeMath engine
                            callback(null, result[random.integer(0, result.length)]._doc);
                        });
                    break;
                case 'id':
                    req.mongoose.account.findOne(
                        {
                            _id: ObjectId(req.query.account.trim())
                        },
                        function (err, result) {
                            if (err) {
                                req.logger.error(err);
                                callback(err);
                                return;
                            }
                            if (result === null) {
                                callback(new Error('not found account'));
                                return;
                            }
                            callback(null, result._doc);
                        });
                    break;
                case 'data':
                    try {
                        var data = JSON.parse(req.query.account.trim());
                    } catch (e) {
                        callback(e);
                        return;
                    }
                    callback(null, data);
                    break;
            }
        },
        function (account, callback) {
            var list = ['country', 'login', 'password'];
            var acc = {};
            list.forEach(function (k) {
                acc[k] = account[k];
            });
            callback(null, acc);
        },
        function (account, callback) {
            req.mongoose.app.create(
                {
                    host: req.query.host.trim().toLowerCase(),
                    account: account
                },
                callback);
        }
    ], function (err, result) {
        if (err) {
            res.status(404).json({error: err, action: req.action, status: 'error'});
            return;
        }
        res.json({result: result, action: req.action, status: 'success'});
    });
}
function view(req, res, next) {
    req.mongoose.app.find({}, function (err, result) {
        if (err) {
            res.status(404).json({error: err, action: req.action});
            return;
        }
        res.json({result: result, action: req.action});
    });
}

function remove(req, res, next) {
    req.mongoose.app.remove({_id: ObjectId(req.query._id)}, function (err) {
        if (err) {
            res.status(404).json({error: err, action: req.action});
            return;
        }
        res.json({result: true, action: req.action});
    });
}
function edit(req, res, next) {
    async.waterfall([
            function (callback) {
                req.mongoose.app.findOne({_id: ObjectId(req.query.id.trim())}, callback);
            },
            function (callback) {
                switch (req.query.type) {
                    default:
                    case 'rand':
                        req.mongoose.account.find(
                            {
                                country: {$exists: true},
                                lang: {$exists: true},
                                access_token_key: {$exists: true},
                                access_token_secret: {$exists: true}
                            },
                            function (err, result) {
                                if (err) {
                                    req.logger.error(err);
                                    callback(err);
                                    return;
                                }
                                if (result.length === 0) {
                                    callback(new Error('not found accounts'));
                                    return;
                                }
                                var random = require("random-js")(); // uses the nativeMath engine
                                callback(null, result[random.integer(0, result.length)]._doc);
                            });
                        break;
                    case 'id':
                        req.mongoose.account.findOne(
                            {
                                _id: ObjectId(req.query.account.trim())
                            },
                            function (err, result) {
                                if (err) {
                                    req.logger.error(err);
                                    callback(err);
                                    return;
                                }
                                if (result === null) {
                                    callback(new Error('not found account'));
                                    return;
                                }
                                callback(null, result._doc);
                            });
                        break;
                    case 'data':
                        try {
                            var data = JSON.parse(req.query.account.trim());
                        } catch (e) {
                            callback(e);
                            return;
                        }
                        callback(null, data);
                        break;
                }
            },
            function (account, callback) {
                var list = ['country', 'login', 'password'];
                var acc = {};
                list.forEach(function (k) {
                    acc[k] = account[k];
                });
                callback(null, acc);
            },
            function (account, callback) {
                var model = req.mongoose.app;
                model.findOne({_id: ObjectId(req.query.id.trim())}, function (err, result) {
                    if (err) {
                        callback(err);
                        return;
                    }
                    if (result === null) {
                        callback(new Error('not found app'));
                        return;
                    }
                    var data = {_id: result._id, host: result.host, account: account};
                    model.update({_id: result._id}, data, function (err) {
                        if (err) {
                            callback(err);
                            return;
                        }
                        callback(null, data);
                    });
                });
            }
        ],
        function (err, result) {
            if (err) {
                res.status(404).json({error: err, action: req.action, status: 'error'});
                return;
            }
            res.json({result: result, action: req.action, status: 'success'});
        });
}
function removeAll(req, res, next) {
    req.mongoose.app.remove({
        login: {
            '$in': req.query._id.map(function (id) {
                return ObjectId(id);
            })
        }
    }, function (err) {
        if (err) {
            res.status(404).json({error: err, action: req.action});
            return;
        }
        res.json({result: true, action: req.action});
    });
}
/**
 *
 * https://www.npmjs.com/package/fast-csv
 */

module.exports = {
    create: add,
    list: view,
    change: edit,
    remove: remove,
    remove_all: removeAll
};
