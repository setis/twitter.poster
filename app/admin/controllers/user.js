function add(req, res, next) {
    if (req.session.root === false) {
        res.status(403).json({error: 'not access root', action: req.action});
        return;
    }
    req.mongoose.user.create({
        login: req.query.login,
        password: req.query.password,
        mode: (req.query.mode) ? true : false
    }, function (err, result) {
        if (err) {
            res.status(404).json({error: err, action: req.action});
            return;
        }
        res.json({result: result, action: req.action});
    });
}
function view(req, res, next) {
    var find =  (req.session.root === false)?{id: req.session.uid}:{};
    req.mongoose.user.find(find, function (err, result) {
        if (err) {
            res.status(404).json({error: err, action: req.action});
            return;
        }
        res.json({result: result, action: req.action});
    });
}
function remove(req, res, next) {
    if (req.session.root === false) {
        res.status(403).json({error: 'not access root', action: req.action});
        return;
    }
    req.mongoose.user.findOne({id: req.query.id}, function (err, result) {
        if (err) {
            res.status(404).json({error: err, action: req.action});
            return;
        }
        result.remove(function (err) {
            if (err) {
                res.status(404).json({error: err, action: req.action});
                return;
            }
            res.json({result: true, action: req.action});
        });
    });
}
function search(req, res, next) {
    if (req.session.root === false) {
        res.status(403).json({error: 'not access root', action: req.action});
        return;
    }
    req.mongoose.user.find({'$or': [{id: '/.*' + req.query.search + '.*/i'}, {login: '/.*' + req.query.search + '.*/i'}]}, function (err, result) {
        if (err) {
            res.status(404).json({error: err, action: req.action});
            return;
        }
        res.json({result: result, action: req.action});
    });
}
function edit(req, res, next) {
    if (req.session.root === false && Number(req.query.id) !== req.session.uid) {
        res.status(403).json({error: 'not access root', action: req.action});
        return;
    }
    var data = {
        login: req.query.login,
        password: req.query.password,
        mode: req.query.mode
    };
    if(req.session.root === false ){
        delete data.mode;
    }
    var model = req.mongoose.user;
    model.update({id: req.query.id}, {
        '$set': data
    }, function (err, result) {
        if (err) {
            res.status(404).json({error: err, action: req.action});
            return;
        }
        model.findOne({id: req.query.id}, function (err, result) {
            if (err) {
                res.status(404).json({error: err, action: req.action});
                return;
            }
            res.json({result: result, action: req.action});
        });

    });
}
function removeAll(req, res, next) {
    if (req.session.root === false) {
        res.status(403).json({error: 'not access root', action: req.action});
        return;
    }
    req.mongoose.user.remove({id: {'$in': req.query.id}}, function (err) {
        if (err) {
            res.status(404).json({error: err, action: req.action});
            return;
        }
        res.json({result: true, action: req.action});
    });
}
module.exports = {
    create: add,
    list: view,
    change: edit,
    remove: remove,
    remove_all: removeAll,
    search: search
};
