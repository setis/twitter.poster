/**
 * Created by alex on 10.07.16.
 */
var config = require('../../config/oauth.json'),
    bodyParser = require("body-parser"),
    express = require('express'),
    session = require('express-session');
var twitterAuth = require('twitter-oauth')(config);
app = express();
app.enable('trust proxy');
app.disable('x-powered-by');
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(session(config.session));
app.get('/', function (req, res) {
    res.redirect(config.login);
});
app.get(config.completeCallback, twitterAuth.middleware, function (req, res) {
    var client = new Twitter({
        consumer_key: cfg.consumer_key,
        consumer_secret: cfg.consumer_secret,
        access_token_key: req.session.oauthAccessToken,
        access_token_secret: req.session.oauthAccessTokenSecret

    });
    client.get('account/verify_credentials', function (error, tweets, response) {
        if (error) {
            req.session.user = {status: 'error', error: error, tweets: tweets, response: response};
        } else {
            req.session.user = {status: 'success', result: tweets};
        }

        res.json({
            access_token_key: req.session.oauthAccessToken,
            access_token_secret: req.session.oauthAccessTokenSecret,
            ip: req.headers['X-Real-IP'] || req.ip,
            user: req.session.user
        });
    });
});
app.get(config.login, twitterAuth.oauthConnect);
app.get(config.loginCallback, twitterAuth.oauthCallback);
app.get(config.logout, twitterAuth.logout);
app.use(function (req, res, next) {
    res.status(404).end();
});
app.listen(config.listen.port, config.listen.ip, function () {
    console.log("oauth start server");
});