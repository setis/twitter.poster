/**
 * Created by alex on 19.04.16.
 */
var casper = require("casper").create({
    viewportSize: {
        width: 1024,
        height: 768
    },
    timeout: 90e3,
    // logLevel: "debug",
    verbose: true
});
var server = 'https://mobile.twitter.com/session/new',
    api = 'http://oauth.arabic-shop.biz',
    server2 = 'https://bitly.com/a/add_twitter_account?rd=%2F',
    api2 = 'https://app.bitly.com/bitlinks/',
    login = casper.cli.get('login'),
    ua = casper.cli.get('ua'),
    password = casper.cli.get('password');
var data = {};

casper.start();
casper.userAgent(ua);
casper.on('page.initialized', function (page) {
    page.evaluate(function () {
        (function () {
            var create = document.createElement;
            document.createElement = function (tag) {
                var elem = create.call(document, tag);
                if (tag === "video") {
                    elem.canPlayType = function () {
                        return "probably";
                    };
                }
                return elem;
            };
        })();
    });
});
casper
    .on('http.status.501', function (resource) {
        this.echo(JSON.stringify({error: 'http.status.501'}));
        this.exit();
    })
    .on('page.error', function (msg) {
        this.echo(JSON.stringify({error: msg}));
        this.exit();
    })
    .on('error', function (msg) {
        if(msg ==='CasperError: Errors encountered while filling form: no field matching names selector "session[username_or_email]" in form; no field matching names selector "session[password]" in form'){
            this.echo(JSON.stringify({error: 'not form loaded'}));
        }else{
            this.echo(JSON.stringify({error: msg}));
        }
        this.capture("logs/screen-"+login+".png");
        var fs  = require('fs');
        fs.write('logs/bug-'+login+'.html',this.getPageContent());
        this.exit();
    });
casper
    .open('https://twitter.com/login')
    .wait(1e3, function () {
        if('https://twitter.com/' === this.getCurrentUrl() ){
            return;
        }
        if (this.exists('form[action="https://twitter.com/sessions"]')) {
            this.evaluate(function (login, password) {
                var form = document.querySelector('form[action="https://twitter.com/sessions"]');
                form.querySelector("input[name='session[username_or_email]']").value = login;
                form.querySelector("input[name='session[password]']").value = password;
                form.querySelector('input[type="submit"]').click();
            }, login, password);
            this.wait(10e3, function () {
                var re = /\/login\/error\?username_or_email/gmi;
                if (re.test(this.getCurrentUrl())) {
                    this.emit('error','login and pasword');
                    return;
                }
                var re2 = /\/account\/login_challenge/gmi;
                if (re2.test(this.getCurrentUrl())) {
                    this.emit('error','confirmation code');
                    return;
                }
            });
        }

    })
    .then(function(){

        var re = /\/login\/error\?username_or_email/gmi;
        if (re.test(this.getCurrentUrl())) {
            this.emit('error','login and pasword');
            return;
        }
        var re2 = /\/account\/login_challenge/gmi;
        if (re2.test(this.getCurrentUrl())) {
            this.emit('error','confirmation code');
            return;
        }
        if('https://twitter.com/' !== this.getCurrentUrl() ){
            this.emit('error','not auth');
            return;
        }
    })
    // .open(server)
    // .wait(5e3, function () {
    //     if (this.exists('form')) {
    //         this.fill('form', {
    //             'session[username_or_email]': login,
    //             'session[password]': password
    //         }, true);
    //     } else {
    //         // this.fillSelectors('form[action="/sessions"]', {
    //         //     'input[name="session[username_or_email]"]': login,
    //         //     'input[name="session[password]"]': password
    //         // }, true);
    //         // this.evaluate(function (login,password) {
    //         //     document.getElementById("session[username_or_email]").value = login;
    //         //     document.getElementById("session[password]").value = password;
    //         //     document.querySelector('input[name="commit"]').click();
    //         // },login,password);
    //         this.echo(JSON.stringify({error: 'not form loaded'}));
    //         this.exit();
    //     }
    // })
    // .wait(5e3, function () {
    //     var re = /\/login\/error\?username_or_email/gmi;
    //     if (re.test(this.getCurrentUrl())) {
    //         this.echo(JSON.stringify({error: 'login and pasword'}));
    //         this.exit();
    //     }
    //     var re2 = /\/account\/login_challenge/gmi
    //     if (re2.test(this.getCurrentUrl())) {
    //         this.echo(JSON.stringify({error: 'confirmation code'}));
    //         this.exit();
    //     }
    //
    // })
    .thenOpen(api)
    .wait(5e3,function(){
        this.click('#allow');

    })
    .wait(15e3, function () {
        data = JSON.parse(this.getPageContent());
        data.login = login;
        data.password = password;
    })

    // .thenOpen(server2, function () {
    //     if (debug) {
    //         this.capture("oauth2.png");
    //         this.echo("Saved oauth of " + (this.getCurrentUrl()));
    //     }
    //     this.click('#allow');
    // })

    // .thenOpen(api2)
    // .wait(5e3, function () {
    //     // this.capture("finish.png");
    //     data.bitly = this.evaluate(function () {
    //         return App.Stores.session.user.cursor().apiKey;
    //     });
    // })
    .run(function () {
        this.echo(JSON.stringify(data));
        this.exit();
    });
