/**
 * Created by alex on 19.04.16.
 */
var casper = require("casper").create({
    viewportSize: {
        width: 1024,
        height: 768
    },
    timeout: 90e3,
    // logLevel: "debug",
    verbose: false
});
// var login = "is.set@mail.ru", password = "z234339013";
var
    login = casper.cli.get('login'),
    ua = casper.cli.get('ua'),
    logs = casper.cli.get('logs') || "logs/",
    msg = casper.cli.get('msg'),
    img = casper.cli.get('files') || [],
    password = casper.cli.get('password');
var data = {};
casper.on('page.initialized', function (page) {
    page.evaluate(function () {
        (function () {
            var create = document.createElement;
            document.createElement = function (tag) {
                var elem = create.call(document, tag);
                if (tag === "video") {
                    elem.canPlayType = function () {
                        return "probably";
                    };
                }
                return elem;
            };
        })();
    });
});
casper
    .on('http.status.501', function (resource) {
        this.echo(JSON.stringify({status: false, msg: 'http.status.501'}));
        this.capture(logs + "screen-" + login + ".png");
        var fs = require('fs');
        fs.write(logs + 'bug-' + login + '.html', this.getPageContent());
        this.exit();
    })
    .on('page.error', function (msg) {
        this.echo(JSON.stringify({status: false, msg: msg,'event':"page.error"}));
        this.capture(logs + "screen-" + login + ".png");
        var fs = require('fs');
        fs.write(logs + 'bug-' + login + '.html', this.getPageContent());
        this.exit();
    })
    .on('error', function (msg) {
        if (msg === 'CasperError: Errors encountered while filling form: no field matching names selector "session[username_or_email]" in form; no field matching names selector "session[password]" in form') {
            this.echo(JSON.stringify({status: false, msg: 'not form loaded'}));
        }
        else {
            this.echo(JSON.stringify({status: false, msg: msg}));
        }
        this.capture(logs + "screen-" + login + ".png");
        var fs = require('fs');
        fs.write(logs + 'bug-' + login + '.html', this.getPageContent());
        this.exit();
    });
casper.start();
casper
    .open('https://twitter.com/login')
    .wait(1e3, function () {
        if('https://twitter.com/' === this.getCurrentUrl() ){
            return;
        }
        if (this.exists('form[action="https://twitter.com/sessions"]')) {
            this.evaluate(function (login, password) {
                var form = document.querySelector('form[action="https://twitter.com/sessions"]');
                form.querySelector("input[name='session[username_or_email]']").value = login;
                form.querySelector("input[name='session[password]']").value = password;
                form.querySelector('input[type="submit"]').click();
            }, login, password);
            this.wait(1e3, function () {
                var re = /\/login\/error\?username_or_email/gmi;
                if (re.test(this.getCurrentUrl())) {
                    // this.echo(JSON.stringify({error: 'login and pasword'}));
                    this.emit('error','login and pasword');
                    // this.exit();
                }
                var re2 = /\/account\/login_challenge/gmi;
                if (re2.test(this.getCurrentUrl())) {
                    // this.echo(JSON.stringify({error: 'confirmation code'}));
                    this.emit('error','confirmation code');
                    // this.exit();
                }
            });
        } else {
            var re = /\/login\/error\?username_or_email/gmi;
            if (re.test(this.getCurrentUrl())) {
                // this.echo(JSON.stringify({error: 'login and pasword'}));
                this.emit('error','login and pasword');
                // this.exit();
            }
            var re2 = /\/account\/login_challenge/gmi;
            if (re2.test(this.getCurrentUrl())) {
                // this.echo(JSON.stringify({error: 'confirmation code'}));
                this.emit('error','confirmation code');
                // this.exit();
            }
        }

    })
    // .waitForSelector('global-new-tweet-button',function(){
    //     this.evaluate(function (msg) {
    //         document.getElementById('global-new-tweet-button').click();
    //         document.querySelector('#tweet-box-global').innerHTML = "<div>" + msg + "</div>";
    //     }, msg);
    //     // }, msg +(new Date()).toTimeString());
    //     if (img.length > 0) {
    //         for(var i in img){
    //             var file = img[i];
    //             casper.page.uploadFile('#global-tweet-dialog-dialog > div.modal-content > div.modal-tweet-form-container > form > div.TweetBoxToolbar > div.TweetBoxExtras.tweet-box-extras > span.TweetBoxExtras-item.TweetBox-mediaPicker > div > div > label > input[type="file"]', file);
    //         }
    //     }
    //     this.wait(3e3, function () {
    //         this.evaluate(function () {
    //             document.querySelector('#global-tweet-dialog-dialog > div.modal-content > div.modal-tweet-form-container > form > div.TweetBoxToolbar > div.TweetBoxToolbar-tweetButton.tweet-button > button').click();
    //         });
    //     })
    // })
    .wait(7e3, function () {
        this.evaluate(function (msg) {
            document.getElementById('global-new-tweet-button').click();
            document.querySelector('#tweet-box-global').innerHTML = "<div>" + msg + "</div>";
        }, msg);
        // }, msg +(new Date()).toTimeString());
        if (img.length > 0) {
            for(var i in img){
                var file = img[i];
                casper.page.uploadFile('#global-tweet-dialog-dialog > div.modal-content > div.modal-tweet-form-container > form > div.TweetBoxToolbar > div.TweetBoxExtras.tweet-box-extras > span.TweetBoxExtras-item.TweetBox-mediaPicker > div > div > label > input[type="file"]', file);
            }
        }
        this.wait(3e3, function () {
            this.evaluate(function () {
                document.querySelector('#global-tweet-dialog-dialog > div.modal-content > div.modal-tweet-form-container > form > div.TweetBoxToolbar > div.TweetBoxToolbar-tweetButton.tweet-button > button').click();
            });
        })
    })
    .wait(10e3, function () {
        this.capture(logs + "screen-" + login + "-pre.png");
        var fs = require('fs');
        fs.write(logs + 'bug-' + login + '-pre.html', this.getPageContent());
        var msg = this.evaluate(function () {
            return $('#message-drawer > div > div > span').text();
        }).trim();
        data.login = login;
        data.msg = msg;
        switch (msg) {
            //ru
            case "Ваш твит опубликован!":
            case "Вы уже отправили этот твит.":
            //bg
            case "Вече си изпратил този туит.":
            case "Туитът ти беше пуснат!":
            //sr
            case "Твој твит је објављен!":
            case "Већ си послао/ла овај твит.":
                //uk
            case "Ви вже надіслали цей твіт.":
            case "Ваш твіт опубліковано!":
                //id
            case "Anda sudah mengirim Tweet ini.":
            case "Tweet Anda terkirim!":
                //de
            case "Dein Tweet wurde gesendet!":
            case "Du hast diesen Tweet bereits gesendet.":
                //en
            case "You have already sent this Tweet.":
            case "Your Tweet was posted!":
                //ja
            case "このツイートはすでに送信済みです。":
            case "ツイートが送信されました。":
                //zh-tw
            case "你的推文已經發布!":
            case "":
                //
            case "":
            case "":
                //
            case "":
            case "":
                //
            case "":
            case "":
                //
            case "":
            case "":
            default:
                data.status = true;
                break;

        }
    })
    .run(function () {
        this.capture(logs + "screen-" + login + "-finish.png");
        var fs = require('fs');
        fs.write(logs + 'bug-' + login + '-finish.html', this.getPageContent());
        this.echo(JSON.stringify(data));
        this.exit();
    });
