/**
 * Created by alex on 28.05.16.
 */
var cluster = require('cluster');

cluster.setupMaster({exec: __dirname + '/index.js'});

for (var i = 0; i < 8; i++) {
    cluster.fork();
}

cluster.on('exit', function(worker, code, signal) {
    console.log('worker ' + worker.process.pid + ' died');
    cluster.fork();
});