/**
 * Created by alex on 20.04.16.
 */
var logger = require('./logger'),
    spawn = require('child_process').spawn,
    Twitter = require('twitter'),
    async = require('async'),
    fs = require('fs'),
    path = require('path'),
    ObjectId = require('mongoose').Types.ObjectId,
    imgDir = require('../../config/admin.json').upload.dest,
    gearmanode = require('gearmanode'),
    worker = gearmanode.worker(),
    random_useragent = require('random-useragent'),
    request = require('request'),
    Agent = require('socks5-https-client/lib/Agent');
gearmanode.Worker.logger.transports.console.level = 'info';
var proxyPort, proxyCountry
var watch = path.normalize(__dirname +  '/../tor/watch');
function proxy() {
    fs.readFile(watch, function (err, data) {
        if (err) {
            return;
        }
        logger.info('load file:%s %s', watch, data.toString());
        var arr = data.toString().split(':');
        proxyPort = Number(arr[0]);
        proxyCountry = arr[1].toUpperCase();
    });
}
proxy();
setInterval(proxy, 15e3);
// fs.watch(path, {encoding: 'string'}, function (event, filename) {
//     logger.info('watch event:%s file:%s ',event,filename);
//     proxy();
// });
require('./loader')(function (core) {
    worker.addFunction('twitter.auth', function (job) {
        var data = JSON.parse(job.payload.toString("utf-8"));
        if (data.country=== proxyCountry) {
            logger.info('auth.begin', data.login, proxyCountry, proxyPort);
            var params = [
                __dirname + '/auth.js',
                '--login=' + data.login,
                '--password=' + data.password,
                // '--ua="' + data.ua + '"',
                '--proxy=127.0.0.1:' + proxyPort,
                '--proxy-type=socks5',
                // '--max-disk-cache-size=2000',
                // '--disk-cache=true',
                '--cookies-file=' + __dirname + '/cookies/' + data.login
            ];
            logger.info(params.join(' '));
            var child = spawn('/usr/bin/casperjs', params);
            child.stdout.on('data', function (raw) {
                console.log(raw.toString(), data.login);
                try {
                    var d = JSON.parse(raw.toString());
                } catch (e) {
                    logger.error('task.auth', e, data.login);
                    core.models.odm.account.update(
                        {_id: ObjectId(data._id)},
                        {
                            $set: {
                                error: {
                                    msg: e.message,
                                    time: new Date()
                                }
                            }
                        }, function (err) {
                            if (err) {
                                logger.error('task.auth', err.message)
                            }
                        });
                    job.reportException(e);
                    return;
                }
                if (d.error) {

                    core.models.odm.account.update(
                        {_id: ObjectId(data._id)},
                        {
                            $set: {
                                error: {
                                    msg: d.error,
                                    time: new Date()
                                }
                            }
                        }, function (err) {
                            if (err) {
                                logger.error('task.auth', err.message)
                            }
                            job.workComplete(data.toString());
                        });
                    return;
                }
                var update = {
                    time: new Date(),
                    access_token_key: d.access_token_key,
                    access_token_secret: d.access_token_secret,
                    // bitly: d.bitly,
                    // ip: proxy.ip
                };
                if (d.user.status === 'success') {
                    update.info = d.user.result;
                    update.lang = d.user.result.lang;
                }

                core.models.odm.account.update({_id: ObjectId(data._id)}, {$set: update}, function (err) {
                    if (err) {
                        logger.error('task.auth', err.message || err)
                    }
                    job.workComplete(data.toString());
                });
                // logger.info('twitter.auth', data.toString());
            });
            child.stderr.on('data', function (data) {
                logger.error('twitter.auth', data.toString());
                job.reportError(data.toString());
            });
            child.on('exit', function (code) {
                logger.info('auth.end', data.login);
            });
        } else {
            job.workComplete();
        }
    });
    worker.addFunction('twitter.post', function (job) {
        var data = JSON.parse(job.payload.toString("utf-8"));
        logger.info(job.payload.toString());
        if (data.account.country === proxyCountry) {
            logger.info('post.begin', data.account.login, proxyCountry, proxyPort);
            var params = [
                __dirname + '/post.js',
                '--proxy=127.0.0.1:' + proxyPort,
                '--proxy-type=socks5',
                '--cookies-file=' + __dirname + '/cookies/' + data.account.login,
                '--log-dir="' + __dirname + '/logs/"',
                '--max-disk-cache-size=2000',
                '--disk-cache=true',
                '--login=' + data.account.login,
                '--password=' + data.account.password,
                '--ua="' + data.account.ua + '"',
                '--msg="' + data.msg + '"',
                '--files="' + data.img + '"'
            ];
            if(data.img.length > 0){
                params.push('--files="' + data.img.map(function(v){return imgDir+'/'+v;}).join(',')+'"');
            }
            logger.info(params.join(' '));
            var child = spawn('/usr/bin/casperjs', params);
            child.stdout.on('data', function (raw) {
                console.log(raw.toString(), data.login);
                try {
                    var d = JSON.parse(raw.toString());
                } catch (e) {
                    logger.error('task.post', e, data.login);
                    core.models.odm.job.update(
                        {_id: ObjectId(data.job)},
                        {
                            $set: {
                                status: false,
                                time: new Date(),
                                result: {
                                    error: e,
                                    msg: raw.toString()
                                }
                            },
                            $inc: {try: 1},

                        }, function (err) {
                            if (err) {
                                logger.error('task.post', err.message)
                            }
                        });

                    job.reportException(e);
                    return;
                }
                core.models.odm.job.update(
                    {_id: ObjectId(data.job)},
                    {
                        $set: {
                            status: d.status,
                            time: new Date(),
                            result: d
                        },
                        $inc: {try: 1},

                    }, function (err) {
                        if (err) {
                            logger.error('task.post', err.message)
                        }
                        job.workComplete(data.toString());
                    });
            });
            child.stderr.on('data', function (data) {
                logger.error('twitter.post', data.toString());
                job.reportError(data.toString());
            });
            child.on('exit', function (code) {
                logger.info('post.end', data.login);
            });
        } else {
            logger.info('post.begin not found proxy ', data.account.login, proxyCountry, proxyPort);
            job.workComplete();
        }
    });
});

