/**
 * Created by alex on 17.06.16.
 */
var fs = require('fs'),
    async = require('async'),
    mustache = require('mustache'),
    spawn = require('child_process').spawn;


var country = ["af", "ax", "al", "dz", "as", "ad", "ao", "ai", "aq", "ag", "ar", "am", "aw", "au", "at", "az", "bs", "bh", "bd", "bb", "by", "be", "bz", "bj", "bm", "bt", "bo", "bq", "ba", "bw", "bv", "br", "io", "bn", "bg", "bf", "bi", "kh", "cm", "ca", "cv", "ky", "cf", "td", "cl", "cn", "cx", "cc", "co", "km", "cg", "cd", "ck", "cr", "ci", "hr", "cu", "cw", "cy", "cz", "dk", "dj", "dm", "do", "ec", "eg", "sv", "gq", "er", "ee", "et", "fk", "fo", "fj", "fi", "fr", "gf", "pf", "tf", "ga", "gm", "ge", "de", "gh", "gi", "gr", "gl", "gd", "gp", "gu", "gt", "gg", "gn", "gw", "gy", "ht", "hm", "va", "hn", "hk", "hu", "is", "in", "id", "ir", "iq", "ie", "im", "il", "it", "jm", "jp", "je", "jo", "kz", "ke", "ki", "kp", "kr", "kw", "kg", "la", "lv", "lb", "ls", "lr", "ly", "li", "lt", "lu", "mo", "mk", "mg", "mw", "my", "mv", "ml", "mt", "mh", "mq", "mr", "mu", "yt", "mx", "fm", "md", "mc", "mn", "me", "ms", "ma", "mz", "mm", "na", "nr", "np", "nl", "nc", "nz", "ni", "ne", "ng", "nu", "nf", "mp", "no", "om", "pk", "pw", "ps", "pa", "pg", "py", "pe", "ph", "pn", "pl", "pt", "pr", "qa", "re", "ro", "ru", "rw", "bl", "sh", "kn", "lc", "mf", "pm", "vc", "ws", "sm", "st", "sa", "sn", "rs", "sc", "sl", "sg", "sx", "sk", "si", "sb", "so", "za", "gs", "ss", "es", "lk", "sd", "sr", "sj", "sz", "se", "ch", "sy", "tw", "tj", "tz", "th", "tl", "tg", "tk", "to", "tt", "tn", "tr", "tm", "tc", "tv", "ug", "ua", "ae", "gb", "us", "um", "uy", "uz", "vu", "ve", "vn", "vg", "vi", "wf", "eh", "ye", "zm", "zw"],
    dir = __dirname + '/tor',
    i = 0,
    control = 10002,
    port = 9052
    ;

function build(path) {
    var tmp, listen = {};
    async.series([
        function (callback) {
            fs.readFile(__dirname + '/config', function (err, data) {
                if (err) {
                    throw err;
                }
                tmp = data.toString();
                callback();
            });
        },
        function (callback) {
            var path = dir + '/etc';
            fs.exists(path, function (status) {
                if (status) {
                    callback();
                    return;
                }
                fs.mkdir(path, callback);
            });
        },

        function (callback) {
            async.each(
                country,
                function (country, callback) {
                    async.series([
                        function (callback) {
                            var path = dir + '/' + country;
                            fs.exists(path, function (status) {
                                if (status) {
                                    callback();
                                    return;
                                }
                                fs.mkdir(path, callback);
                            });
                        },
                        function (callback) {
                            var path = dir + '/' + country + '/data';
                            fs.exists(path, function (status) {
                                if (status) {
                                    callback();
                                    return;
                                }
                                fs.mkdir(path, callback);
                            });
                        },
                        function (callback) {
                            var path = dir + '/' + country + '/log';
                            fs.exists(path, function (status) {
                                if (status) {
                                    callback();
                                    return;
                                }
                                fs.mkdir(path, callback);
                            });
                        }

                    ], function (err) {
                        if (err) {
                            throw err;
                        }
                        i++;
                        var conf = mustache.render(tmp, {
                            port: port + i,
                            control: control + i,
                            log: dir + '/' + country + '/log',
                            data: dir + '/' + country + '/data',
                            country: '{' + country + '}',
                            pid: dir + '/' + country + '/' + country + '.pid'
                        });
                        listen[country] = port + i
                        fs.writeFile(dir + '/etc/' + country, conf, callback);
                    });
                }, callback);
        }
    ], function (err) {
        if (err) {
            throw err;
        }
        fs.writeFile(path, JSON.stringify(listen), function (err) {
            if (err) {
                throw err;
            }
        })
    });

}
build('./tor/listen.json');
setTimeout(function () {
    run(require('./tor/listen.json'), __dirname + '/watch', 60*5, ['jp', 'us', 'gd', 'ca']);
    // run(require('./tor/listen.json'), __dirname + '/watch', 60*2);
}, 5e3);
// runAll();
function runAll() {
    async.each(country, function (country, callback) {
        var cmd = '/usr/sbin/tor -f ' + dir + '/etc/' + country;
        exec(cmd);
        callback();
    });

}
function run(list, file, time, country) {
    var interval = time * 1e3;
    if (country === undefined || (country instanceof Array && country.length < 1 )) {
        country = Object.keys(list);
    }
    async.eachSeries(
        country,
        function (country, callback) {
            var child = spawn('/usr/sbin/tor', ['-f', dir + '/etc/' + country]);
            fs.writeFile(file, list[country] + ':' + country, function (err) {
                if (err) {
                    throw err;
                }
                console.log('connect country:%s port:%s', country, list[country]);
            });
            // child.stdout.on('data', function (data) {
            //     console.log('stdout: ' + data);
            // });
            child.stderr.on('data', function (data) {
                console.log('stdout: ' + data);
            });
            setTimeout(function () {
                setTimeout(function () {
                    // console.log('signal kill');
                    child.kill();
                }, 90);
                callback();
            }, interval);
            child
                .on('close', function (code) {
                    console.log('disconnect country:%s port:%s', country, list[country]);
                })
                // .on('exit', function (code) {
                //     console.log('exit country:%s port:%s', country, list[country]);
                // })
                .on('error', function (err) {
                    console.log('error country:%s port:%s msg:%s code:%s', country, list[country], err.message, err.code);
                });

        },
        function () {
            setTimeout(function () {
                run(list, file, time, country);
            }, interval)

        });
}

